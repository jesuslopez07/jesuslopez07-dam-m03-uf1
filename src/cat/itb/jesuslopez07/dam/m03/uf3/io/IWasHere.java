package cat.itb.jesuslopez07.dam.m03.uf3.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

public class IWasHere {
    public static void main(String[] args) {
        String homePath = System.getProperty("user.home");
        Path home = Path.of(homePath, "m3");
        Path fitxer = home.resolve("i_was_here.txt");

        try{
            OutputStream outputStream = Files.newOutputStream(fitxer, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            PrintStream printStream = new PrintStream(outputStream, true);
            String data = LocalDateTime.now().toString();
            printStream.printf("I Was Here: %s\n", data);
        }catch (IOException e){
            System.out.println("Error al escriure el fitxer: " + fitxer);
        }
    }
}
