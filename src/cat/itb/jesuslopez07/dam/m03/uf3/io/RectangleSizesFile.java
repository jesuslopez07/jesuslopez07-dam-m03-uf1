package cat.itb.jesuslopez07.dam.m03.uf3.io;

import cat.itb.jesuslopez07.dam.m03.uf2.classfun.Rectangle;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

public class RectangleSizesFile {
    public static void main(String[] args) {
        // Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //Path fitxer = Path.of(scanner.next());
        Path fitxer = Path.of("/home/joapuiib/m3/rectangles.txt");

        try{
            Scanner scannerFitxer = new Scanner(fitxer);

            int mida = scannerFitxer.nextInt();
            Rectangle[] rectangles = new Rectangle[mida];

            for(int i = 0; i < rectangles.length; i++)
                rectangles[i] = Rectangle.readRectangle(scannerFitxer);

            for(Rectangle r : rectangles){
                System.out.println(r.getRectangleInfo());
            }

        }catch (IOException e){
            System.out.println(e);
        }
    }
}
