package cat.itb.jesuslopez07.dam.m03.uf3.io;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class EssayAnalyser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String pathFitxer = scanner.next();
        Path fitxer = Path.of(pathFitxer);

        try {
            Scanner scannerFitxer = new Scanner(fitxer);
            int nombreLinies = 0;
            int nombreParaules = 0;

            while (scannerFitxer.hasNextLine()) {
                String line = scannerFitxer.nextLine();
                nombreLinies++;
                System.out.println("Linia: " + line);
                System.out.println("N linies: " + line);

                String[] paraules = line.split(" ");
                System.out.println("Paraules: " + Arrays.toString(paraules));
                nombreParaules += paraules.length;

                System.out.println("Número de línies: " + nombreLinies);
                System.out.println("Número de paraules: " + nombreParaules);
            }

            System.out.println("Número de línies: " + nombreLinies);
            System.out.println("Número de paraules: " + nombreParaules);

        }catch (IOException e){
            e.getMessage();
        }
    }
}
