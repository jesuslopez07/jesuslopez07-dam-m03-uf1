package cat.itb.jesuslopez07.dam.m03.uf3.io;

import cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions.IntegerLists;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class RadarFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        //Path fitxer = Path.of(scanner.next());
        Path fitxer = Path.of("/home/joapuiib/m3/radar.txt");

        List<Integer> velocitats = new ArrayList<Integer>();
        try{
            Scanner scannerFitxer = new Scanner(fitxer);

            while(scannerFitxer.hasNextInt()){
                int velocitat = scannerFitxer.nextInt();
                velocitats.add(velocitat);
            }

        }catch (IOException e){
            System.out.println("Error al llegir les dades del radar.");
        }

        System.out.printf("Velocitat màxima: %d\n", IntegerLists.max(velocitats));
        System.out.printf("Velocitat mínima: %d\n", IntegerLists.min(velocitats));
        System.out.printf("Velocitat mitjana: %.2f\n", IntegerLists.average(velocitats));
    }
}
