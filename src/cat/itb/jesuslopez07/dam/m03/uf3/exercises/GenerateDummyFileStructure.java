package cat.itb.jesuslopez07.dam.m03.uf3.exercises;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

public class GenerateDummyFileStructure {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String homePath = System.getProperty("user.home");
        Path home = Path.of(homePath, "m3");

        Path dummyFolder = home.resolve("dummyfolders");
        try {
            if(!Files.exists(dummyFolder)) {
                Files.createDirectory(dummyFolder);
                System.out.println("Carpeta creada: dummyfolders");
            }else
                System.out.println("Carpeta dummyfolders ja existeix.");
        }catch (IOException io){
            System.out.println("Error creant la carpeta dummyfolders");
        }

        for(int i = 1; i <= 100; i++){
            try {
                Path folder = dummyFolder.resolve(Integer.toString(i));
                if(!Files.exists(folder)) {
                    Files.createDirectory(folder);
                    System.out.printf("Carpeta creada: %d\n", i);
                }else
                    System.out.printf("Carpeta %d ja existeix.\n", i);
            }catch (IOException io){
                System.out.printf("Error creant la carpeta %d\n", i);
            }
        }
    }
}
