package cat.itb.jesuslopez07.dam.m03.uf3.exercises;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Scanner;

public class FileExists {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String path = scanner.nextLine();
        Path file = Path.of(path);

        boolean exists = Files.exists(file);
        System.out.println(exists);
    }
}
