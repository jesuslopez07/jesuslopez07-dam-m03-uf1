package cat.itb.jesuslopez07.dam.m03.uf3.exercises;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.Scanner;

public class ProfileBackup {
    public static void main(String[] args) {
        String homePath = System.getProperty("user.home");
        Path home = Path.of(homePath, "m3");

        String date = LocalDateTime.now().toLocalDate().toString();
        Path backup = Path.of(home.toString(), "backup", date);
        try {
            Files.createDirectories(backup);
            System.out.println("Directori creat: " + backup);
        }catch (IOException e){
            System.out.println("Error creant el directori: " + backup);
        }

        Path profileOrigin = home.resolve(".profile");
        Path profileTarget = backup.resolve(".profile");
        try {
            Files.copy(profileOrigin, profileTarget);
            System.out.printf("Fitxer %s copiat a %s\n", profileOrigin, profileTarget);
        }catch (IOException e){
            System.out.println("Error copiant els fitxers");
        }
    }
}
