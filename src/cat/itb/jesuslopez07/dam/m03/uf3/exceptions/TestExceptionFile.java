package cat.itb.jesuslopez07.dam.m03.uf3.exceptions;

import java.io.EOFException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TestExceptionFile {
    public static void crearFitxer(String path) throws IOException{
        Files.createFile(Paths.get(path));
    }

    public static void main(String[] args) {
        try {
            crearFitxer("/home/joapuiib/file.txt");
        }catch (IOException e){
            System.err.println("Error al crear el fitxer.");
        }
    }
}
