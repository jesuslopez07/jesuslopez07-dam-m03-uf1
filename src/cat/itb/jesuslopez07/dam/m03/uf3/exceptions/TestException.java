package cat.itb.jesuslopez07.dam.m03.uf3.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TestException {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        try{
            int value = scanner.nextInt();
            System.out.println(value);
        }catch (InputMismatchException ex){
            System.err.println("Not a number.");
        }
    }
}
