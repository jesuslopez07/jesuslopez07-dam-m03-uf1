package cat.itb.jesuslopez07.dam.m03.uf3.generalexam;

import cat.itb.jesuslopez07.dam.m03.uf2.dataclasses.StudentGrade;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class StudentGradesFile {
    public static StudentGrade[] readFromFile(Path studentsFile){
        StudentGrade[] notes = null;
        try{
            Scanner fileScanner = new Scanner(studentsFile).useLocale(Locale.US);

            int numeroNotes = fileScanner.nextInt();
            notes = new StudentGrade[numeroNotes];

            for (int i = 0; i < notes.length; i++) {
                notes[i] = StudentGrade.readStudent(fileScanner);
            }

        } catch (IOException e){
            System.out.println("Error al llegir: " + studentsFile);
        }

        return notes;
    }

    public static int calcularAprovats(StudentGrade[] students){
        int aprovats = 0;
        for (int i = 0; i < students.length; i++) {
            if(students[i].getNotaFinal() >= 5)
                aprovats++;
        }
        return aprovats;
    }
    public static void printResum(StudentGrade[] students){
        System.out.println("----- Estudiants -----");
        for (int i = 0; i < students.length; i++) {
            System.out.printf("%s: %.1f\n", students[i].getNom(), students[i].getNotaFinal());
        }

        System.out.println("----- Resum -----");
        int aprovats = calcularAprovats(students);
        int suspesos = students.length - aprovats;
        System.out.printf("Aprovats: %d\n", aprovats);
        System.out.printf("Suspesos: %d\n", suspesos);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String studentsFilePath = scanner.next();
        Path studentsFile = Path.of(studentsFilePath);

        StudentGrade[] students = readFromFile(studentsFile);
        System.out.println(Arrays.toString(students));

        printResum(students);
    }
}
