package cat.itb.jesuslopez07.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class FileCounterLog {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String folderPath = scanner.next();
        Path folder = Path.of(folderPath);

        String homePath = System.getProperty("user.home");
        Path counterLog = Path.of(homePath, "m3", "counterlog.txt");

        try{
            Stream<Path> filesStream = Files.list(folder);
            List<Path> files = filesStream.collect(toList());

            OutputStream outputStream = Files.newOutputStream(counterLog, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            PrintStream printStream = new PrintStream(outputStream, true);

            int nombreFitxers = files.size();
            printStream.printf("%s - Tens %d fitxers a %s\n", LocalDateTime.now().toString(), nombreFitxers, folder);
            System.out.printf("%s - Tens %d fitxers a %s\n", LocalDateTime.now().toString(), nombreFitxers, folder);
        } catch (IOException e){
            System.out.println("Error al llegir: " + folder);
        }
    }
}
