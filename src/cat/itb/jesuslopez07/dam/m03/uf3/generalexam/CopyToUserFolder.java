package cat.itb.jesuslopez07.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Scanner;

public class CopyToUserFolder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String folderPath = scanner.next();
        String nom = scanner.next();
        String cognom = scanner.next();

        Path folder = Path.of(folderPath, cognom, nom);

        if (Files.exists(folder)){
            Path userFolder = folder.resolve(cognom).resolve(nom);
            // Path userFolder = Path.of(folderPath, cognom, nom);
            // Path userFolder = Paths.get(folderPath, cognom, nom);
            try{
                Files.createDirectories(userFolder);
            } catch (IOException e){
                System.out.println("Error al crear els directoris: " + userFolder);
            }

            String homePath = System.getProperty("user.home");
            Path bashrc = Path.of(homePath, ".bashrc");

            Path nouBashrc = userFolder.resolve(".bashrc");

            try{
                Files.copy(bashrc, nouBashrc);
                System.out.println("Fitxer copiat a la carpeta " + userFolder);
            } catch (IOException e){
                System.out.println("Error al crear copiar .bashrc a : " + nouBashrc);
            }
        }
    }
}
