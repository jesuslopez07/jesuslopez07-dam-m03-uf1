package cat.itb.jesuslopez07.dam.m03.uf3.fitxers;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Locale;
import java.util.Scanner;

public class Writer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String homePath = System.getProperty("user.home");
        Path file = Path.of(homePath).resolve("writer.txt");


        try {
            OutputStream outputStream = Files.newOutputStream(file, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            PrintStream printStream = new PrintStream(outputStream, true);

            while(true){
                int nombreOcurrencies = scanner.nextInt();
                if(nombreOcurrencies < 1)
                    break;
                String paraula = scanner.next();

                for (int i = 0; i < nombreOcurrencies; i++){
                    printStream.printf("%d %s\n", i, paraula);
                    System.out.printf("%d %s\n", i, paraula);
                }
            }
        }catch (IOException e){
            System.err.println("Error");
        }
    }
}
