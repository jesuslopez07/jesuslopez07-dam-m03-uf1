package cat.itb.jesuslopez07.dam.m03.uf3.fitxers;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Scanner;

public class Reader {
    public static void main(String[] args) {
        String homePath = System.getProperty("user.home");
        Path file = Path.of(homePath).resolve("writer.txt");

        try {
            Scanner fileScanner = new Scanner(file);
            int nLinia = 1;
            while(fileScanner.hasNextLine()){
                int n = fileScanner.nextInt();
                String line = fileScanner.nextLine();
                System.out.printf("%d: %s %d\n", nLinia, line, n);
                nLinia++;
            }
        } catch (IOException e){
            System.out.println("Error obrint el fitxer");
        }
    }
}
