package cat.itb.jesuslopez07.dam.m03.uf4.abstracts.figure;
import cat.itb.jesuslopez07.dam.m03.uf4.abstracts.interficies.FigureInterface;
public class RectangleFigure extends Figure implements FigureInterface {
    private int width;
    private int height;

    public RectangleFigure(int width, int height, String color){
        super(color);
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw(){
        for (int i = 0; i < height; i++){
            for (int j = 0; j < width; j++){
                System.out.print("X");
            }
            System.out.println();
        }
    }

    @Override
    public double area() {
        return 0;
    }

    @Override
    public double perimeter() {
        return 0;
    }
}
