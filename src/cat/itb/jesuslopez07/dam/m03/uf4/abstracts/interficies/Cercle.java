package cat.itb.jesuslopez07.dam.m03.uf4.abstracts.interficies;

public class Cercle implements FigureInterface {
    private double radi;

    public Cercle(double radi){
        this.radi = radi;
    }

    @Override
    public String toString() {
        return "Cercle{" +
                "radi=" + radi +
                '}';
    }

    public double area(){
        return Math.PI * Math.pow(radi, 2);
    }

    public double perimeter(){
        return 2 * Math.PI * radi;
    }
}
