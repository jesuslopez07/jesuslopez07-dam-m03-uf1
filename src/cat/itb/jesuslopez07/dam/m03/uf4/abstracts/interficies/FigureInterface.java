package cat.itb.jesuslopez07.dam.m03.uf4.abstracts.interficies;

public interface FigureInterface {
    /**
     *
     * @return
     */
    public double area();
    public double perimeter();
}
