package cat.itb.jesuslopez07.dam.m03.uf4.abstracts.figure;

import cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures.ConsoleColors;

public class PaintFigures {
    public static void main(String[] args) {
        Figure f = new RectangleFigure(8, 2, ConsoleColors.GREEN);
        f.draw();
        f.paint();
        Figure f2 = new TriangleFigure(5, ConsoleColors.BLUE);
        f2.draw();
        f2.paint();
    }
}
