package cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures;

public class RectangleFigure extends Figure{
    private int width;
    private int height;

    public RectangleFigure(int width, int height, String color) {
        super(color);
        this.width = width;
        this.height = height;
    }

    @Override
    public void draw(){
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print("X");
            }
            System.out.println();
        }
    }
}