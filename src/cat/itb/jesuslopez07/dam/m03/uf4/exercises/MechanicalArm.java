package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class MechanicalArm {
    private double angle;
    private double height;
    private boolean turnedOn;

    public MechanicalArm(){
        this.angle = 0;
        this.height = 0;
        this.turnedOn = false;
    }

    public double getAngle() {
        return angle;
    }
    public double getHeight() {
        return height;
    }
    public boolean isTurnedOn() {
        return turnedOn;
    }

    private void setAngle(double angle) {
        if(isTurnedOn()){
            this.angle= angle;
            this.angle = Math.max(this.angle, 0);
            this.angle = Math.min(this.angle, 360);
        }
    }
    private void setHeight(double height) {
        if(isTurnedOn())
            this.height = Math.max(height, 0);
    }
    private void setTurnedOn(boolean turnedOn) {
        this.turnedOn = turnedOn;
    }

    public void turnOn(){
        setTurnedOn(true);
    }

    public void turnOff(){
        setTurnedOn(false);
    }

    public void updateHeight(double height){
        setHeight(getHeight() + height);
    }
    public void updateAngle(double angle){
        setAngle(getAngle() + angle);
    }

    @Override
    public String toString() {
        return "MechanicalArm{" +
                "angle=" + angle +
                ", height=" + height +
                ", turnedOn=" + turnedOn +
                '}';
    }
}
