package cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures;

import java.util.ArrayList;
import java.util.List;

public class ThreeRectangles {
    public static void main(String[] args) {
        List<RectangleFigure> rectangles = new ArrayList<RectangleFigure>();

        rectangles.add(new RectangleFigure(5, 4, ConsoleColors.RED));
        rectangles.add(new RectangleFigure(2, 2, ConsoleColors.YELLOW));
        rectangles.add(new RectangleFigure(5, 3, ConsoleColors.GREEN));
        rectangles.add(new RectangleFigure(6, 1, ConsoleColors.CYAN));


        for(RectangleFigure r : rectangles){
            r.paint();
        }

    }
}
