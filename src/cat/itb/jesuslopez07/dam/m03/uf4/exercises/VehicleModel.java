package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class VehicleModel {
    protected String name;
    protected VehicleBrand brand;

    public VehicleModel(String name, VehicleBrand brand) {
        this.name = name;
        this.brand = brand;
    }

    public String getName() {
        return name;
    }
    public VehicleBrand getBrand() {
        return brand;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setBrand(VehicleBrand brand) {
        this.brand = brand;
    }
}
