package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class BicycleBrand {
    private String name;
    private String country;

    public BicycleBrand(String name, String country) {
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString() {
        return "BicycleBrand{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}