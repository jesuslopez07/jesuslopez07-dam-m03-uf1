package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class Vehicles {
    public static void main(String[] args) {
        VehicleBrand brand = new VehicleBrand("lorem", "Togo");

        // Exemple polimorfisme
        VehicleModel v1 = new BicycleModel("modelX", 5, brand);
        VehicleModel v2 = new ScooterModel("s562", 45.3, brand);
        System.out.println(v1);
        System.out.println(v2);

        // v1.getGear() => NO FUNCIONA
        // Casting a la classe BicycleModel per poder utilitzar getGear()
        if(v1 instanceof BicycleModel) {
            BicycleModel b = (BicycleModel) v1;
            b.getGear();
        }
    }
}
