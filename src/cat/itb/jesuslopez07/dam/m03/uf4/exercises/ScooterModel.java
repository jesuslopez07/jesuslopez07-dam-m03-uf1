package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class ScooterModel extends VehicleModel {
    private double power;

    public ScooterModel(String name, double power, VehicleBrand brand) {
        super(name, brand);
        this.power = power;
    }

    @Override
    public String toString() {
        return "ScooterModel{" +
                "name='" + name + '\'' +
                ", power=" + power +
                ", brand=" + brand +
                '}';
    }
}