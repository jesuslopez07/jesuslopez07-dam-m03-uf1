package cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures;

import java.io.PrintStream;

public class RectangleFigureBasic {
    private String color;
    private int width;
    private int height;

    public RectangleFigureBasic(int width, int height, String color) {
        this.color = color;
        this.width = width;
        this.height = height;
    }

    public void paint(){
        System.out.print(color);
        draw();
        System.out.print(ConsoleColors.RESET);
    }

    public void draw(){
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                System.out.print("X");
            }
            System.out.println();
        }
    }
}