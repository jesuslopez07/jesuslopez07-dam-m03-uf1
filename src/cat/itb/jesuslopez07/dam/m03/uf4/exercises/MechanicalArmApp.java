package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class MechanicalArmApp {
    public static void main(String[] args) {
        MechanicalArm arm = new MechanicalArm();
        System.out.println(arm);

        arm.turnOn();
        System.out.println(arm);

        arm.updateHeight(3);
        System.out.println(arm);

        arm.updateAngle(180);
        System.out.println(arm);

        arm.updateHeight(-23);
        System.out.println(arm);

        arm.updateAngle(580);
        System.out.println(arm);

        arm.updateHeight(3);
        System.out.println(arm);

        arm.turnOff();
        System.out.println(arm);

        arm.updateHeight(3);
        System.out.println(arm);
    }
}
