package cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures;

public class InvertedTriangleFigure extends Figure{
    private int base;

    public InvertedTriangleFigure(int base, String color) {
        super(color);
        this.base = base;
    }

    @Override
    public void draw(){
        for (int i = base; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                System.out.print("X");
            }
            System.out.println();
        }
    }
}