package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class BicycleModel extends VehicleModel {
    private int gear;

    public BicycleModel(String name, int gear, VehicleBrand brand) {
        super(name, brand);
        this.gear = gear;
    }

    public int getGear() {
        return gear;
    }

    public void setGear(int gear) {
        this.gear = gear;
    }

    @Override
    public String toString() {
        return "BicycleModel{" +
                "name='" + name + '\'' +
                ", gear=" + gear +
                ", brand=" + brand +
                '}';
    }
}