package cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PrintFigures {
    public static void main(String[] args) {
        List<Figure> figures = new LinkedList<>();

        figures.add(new RectangleFigure(5, 3, ConsoleColors.RED));
        figures.add(new TriangleFigure(5,  ConsoleColors.GREEN));
        figures.add(new TriangleFigure(3,  ConsoleColors.CYAN));
        figures.add(new InvertedTriangleFigure(7,  ConsoleColors.PURPLE));


        for (Figure f : figures){
            f.paint();
        }
    }
}
