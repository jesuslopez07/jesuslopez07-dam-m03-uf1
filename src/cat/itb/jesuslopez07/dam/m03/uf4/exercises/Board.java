package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

import cat.itb.jesuslopez07.dam.m03.uf2.classfun.Rectangle;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private List<Rectangle> rectangles;

    public Board(List<Rectangle> rectangles){
        this.rectangles = rectangles;
    }

    public Board(){
        this( new ArrayList<Rectangle>() );
        // this.rectangles = new ArrayList<Rectangle>();
    }

    public double getTotalArea(){
        double area = 0;
        for(Rectangle rectangle: rectangles){
            area+= rectangle.getArea();
        }
        return area;
    }
    public int countRectangle(){
        return rectangles.size();
    }
}
