package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class Bicycles {
    public static void main(String[] args) {
        BicycleBrand brand = new BicycleBrand("lorem", "Togo");

        BicycleModelBasic m1 = new BicycleModelBasic("modelX", 5, brand);
        BicycleModelBasic m2 = new BicycleModelBasic("model5s628", 7, brand);

        System.out.println(m1);
        System.out.println(m2);
    }
}
