package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class VehicleBrand {
    private String name;
    private String country;

    public VehicleBrand(String name, String country) {
        this.name = name;
        this.country = country;
    }

    @Override
    public String toString() {
        return "VehicleBrand{" +
                "name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}