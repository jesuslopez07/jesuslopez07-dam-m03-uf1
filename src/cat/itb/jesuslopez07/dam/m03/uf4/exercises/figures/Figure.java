package cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures;

public class Figure {
    protected String color;

    public Figure(String color){
        this.color = color;
    }

    protected void prepareColor(){
        System.out.print(this.color);
    }

    protected void clearColor(){
        System.out.print(ConsoleColors.RESET);
    }

    public String getColor(){
        return this.color;
    }

    public String getClearColor() {
        return ConsoleColors.RESET;
    }

    public void paint(){
        prepareColor();
        draw();
        clearColor();
    }

    public void draw(){
        //TODO
    }

    @Override
    public String toString() {
        return "Figure{" +
                "color='" + color + "X" + ConsoleColors.RESET + '\'' +
                '}';
    }
}
