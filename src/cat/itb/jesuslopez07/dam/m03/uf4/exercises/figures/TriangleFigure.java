package cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures;

public class TriangleFigure extends Figure{
    private int base;

    public TriangleFigure(int base, String color) {
        super(color);
        this.base = base;
    }

    @Override
    public void draw(){
        for (int i = 0; i < base; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print("X");
            }
            System.out.println();
        }
    }
}