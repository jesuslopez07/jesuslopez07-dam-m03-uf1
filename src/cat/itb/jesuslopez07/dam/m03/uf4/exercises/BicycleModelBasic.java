package cat.itb.jesuslopez07.dam.m03.uf4.exercises;

public class BicycleModelBasic {
    private String name;
    private int gear;
    private BicycleBrand brand;

    public BicycleModelBasic(String name, int gear, BicycleBrand brand) {
        this.name = name;
        this.gear = gear;
        this.brand = brand;
    }

    @Override
    public String toString() {
        return "BicycleModel{" +
                "name='" + name + '\'' +
                ", gear=" + gear +
                ", brand=" + brand +
                '}';
    }
}