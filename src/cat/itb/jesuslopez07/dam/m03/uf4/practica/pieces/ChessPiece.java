package cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces;

import cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures.ConsoleColors;
import cat.itb.jesuslopez07.dam.m03.uf4.practica.Position;

public abstract class ChessPiece {
    protected boolean white;

    public ChessPiece(boolean white){
        this.white = white;
    }

    public boolean isWhite() {
        return white;
    }

    @Override
    public String toString() {
        String color = white ? ConsoleColors.YELLOW : ConsoleColors.BLUE;
        return color + getPieceString() + ConsoleColors.RESET;
    }

    protected abstract String getPieceString();
    public abstract boolean isMovePossible(Position from, Position to);

}
