package cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces;


import cat.itb.jesuslopez07.dam.m03.uf4.practica.Position;

import static java.lang.Math.abs;

public class Queen extends ChessPiece{
    public Queen(boolean white){
        super(white);
    }

    @Override
    protected String getPieceString() {
        return "♛";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return (from.getRow() == to.getRow() || from.getColumn() == to.getColumn()) ||
                abs(from.getRow() - to.getRow()) == (char)abs(from.getColumn() - to.getColumn());
    }
}

