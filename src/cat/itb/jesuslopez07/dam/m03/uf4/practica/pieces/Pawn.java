package cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces;

import cat.itb.jesuslopez07.dam.m03.uf4.practica.Position;

public class Pawn extends ChessPiece{
    public Pawn(boolean white){
        super(white);
    }

    @Override
    protected String getPieceString() {
        return "♟";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return from.getRow() - to.getRow() == 1;
    }
}

