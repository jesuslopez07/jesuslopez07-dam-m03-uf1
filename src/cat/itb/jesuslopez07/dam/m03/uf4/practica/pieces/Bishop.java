package cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces;

import cat.itb.jesuslopez07.dam.m03.uf4.practica.Position;

public class Bishop extends ChessPiece{
    public Bishop(boolean white){
        super(white);
    }

    @Override
    protected String getPieceString() {
        return "♝";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return (from.getRow() != to.getRow() || from.getColumn() != to.getColumn()) &&
                (from.getRow() - to.getRow()) == (from.getColumn() - to.getColumn());
    }
}

