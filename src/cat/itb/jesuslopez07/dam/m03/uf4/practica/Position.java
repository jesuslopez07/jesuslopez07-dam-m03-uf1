package cat.itb.jesuslopez07.dam.m03.uf4.practica;

public class Position {
    protected char file; // Columna
    protected int rank; // Fila

    public Position(char file, int rank){
        this.rank = rank;
        this.file = file;
    }
    public Position(int i, int j){
        this.rank = 8 - i;
        this.file = (char)('A' + j);
    }

    public Position(String code){
        code = code.toUpperCase();
        this.file = code.charAt(0);
        this.rank = Integer.parseInt(Character.toString(code.charAt(1)));
    }

    public int getRank(){
        return rank;
    }
    public char getFile(){
        return file;
    }

    @Override
    public String toString() {
        return String.format("%c%d", file, rank);
    }

    public int getColumn(){
        return file - 'A';
    }
    public int getRow(){
        return 8 - rank;
    }

}
