package cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces;

import cat.itb.jesuslopez07.dam.m03.uf4.practica.Position;

import static java.lang.Math.abs;

public class King extends ChessPiece{
    public King(boolean white){
        super(white);
    }

    @Override
    protected String getPieceString() {
        return "♚";
    }

    @Override
    public boolean isMovePossible(Position from, Position to) {
        return abs(from.getRow() - to.getRow()) == 1;
    }
}

