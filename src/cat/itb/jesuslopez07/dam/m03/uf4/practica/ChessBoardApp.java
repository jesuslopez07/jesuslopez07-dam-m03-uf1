package cat.itb.jesuslopez07.dam.m03.uf4.practica;

import java.util.Locale;
import java.util.Scanner;

public class ChessBoardApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        ChessBoard board = new ChessBoard();
        while(true){
            board.printBoard();

            String from = scanner.next();

            if(from.equals("-1"))
                break;

            String to = scanner.next();
            board.move(from, to);
        }
    }

}
