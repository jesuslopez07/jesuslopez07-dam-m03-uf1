package cat.itb.jesuslopez07.dam.m03.uf4.practica;

import cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces.Bishop;
import cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces.ChessPiece;
import cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces.King;

public class PrintChessPieceTest {
    public static void main(String[] args) {
        ChessPiece white = new King(true);
        ChessPiece black = new Bishop(false);
        System.out.println(white);
        System.out.println(black);
    }
}

