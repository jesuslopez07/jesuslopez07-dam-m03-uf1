package cat.itb.jesuslopez07.dam.m03.uf4.practica;

import cat.itb.jesuslopez07.dam.m03.uf4.practica.pieces.*;

public class ChessBoard {
    protected ChessPiece board[][];

    public ChessBoard(){
        this.board = new ChessPiece[8][8];

        setPiece("a1", new Rook(true));
        setPiece('B', 1, new Knight(true));
        setPiece('C', 1, new Bishop(true));
        setPiece('D', 1, new Queen(true));
        setPiece('E', 1, new King(true));
        setPiece('F', 1, new Bishop(true));
        setPiece('G', 1, new Knight(true));
        setPiece('H', 1, new Rook(true));
        for(char file = 'A'; file <= 'H'; file++){
            setPiece(file, 2, new Pawn(true));
        }

        setPiece('A', 8, new Rook(false));
        setPiece('B', 8, new Knight(false));
        setPiece('C', 8, new Bishop(false));
        setPiece('D', 8, new Queen(false));
        setPiece('E', 8, new King(false));
        setPiece('F', 8, new Bishop(false));
        setPiece('G', 8, new Knight(false));
        setPiece('H', 8, new Rook(false));
        for(char file = 'A'; file <= 'H'; file++){
            setPiece(file, 7, new Pawn(false));
        }
    }

    public void setPiece(char file, int rank, ChessPiece piece){
        Position pos = new Position(file, rank);
        setPiece(pos, piece);
    }
    public void setPiece(String code, ChessPiece piece){
        Position pos = new Position(code);
        setPiece(pos, piece);
    }
    public void setPiece(Position pos, ChessPiece piece){
        this.board[pos.getRow()][pos.getColumn()] = piece;
    }

    public void printBoard(){
        for(int i = 0; i < 8; i++){
            for(int j = 0; j < 8; j++){
                String piece = board[i][j] == null ? " " : board[i][j].toString();
                System.out.print(piece);
            }
            System.out.println();
        }
    }

    public ChessPiece getPiece(Position pos){
        return this.board[pos.getRow()][pos.getColumn()];
    }

    public void move(String fromCode, String toCode){
        Position from = new Position(fromCode);
        Position to = new Position(toCode);
        move(from, to);
    }
    private void move(Position from, Position to){
        ChessPiece piece = getPiece(from);
        if(piece == null)
            return;

        boolean possible = true;

        if(!piece.isMovePossible(from, to)) {
            possible = false;
            System.out.println("Moviment impossible!");
        }

        if (possible) {
            setPiece(from, null);
            setPiece(to, piece);
        }
    }

}
