package cat.itb.jesuslopez07.dam.m03.uf4.generalexam.gameenemies;

public abstract class Enemy {
    protected String name;
    protected int life;

    public Enemy(String name, int life) {
        this.name = name;
        this.life = life;
    }

    public String getName() {
        return name;
    }
    public int getLife() {
        return life;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void setLife(int life) {
        this.life = Math.max(0, life);
    }
    public void substractLife(int life){
        setLife(getLife() - life);
    }

    abstract void resolveAtack(int strength);

    public void attack(int strength){
        if(life == 0){
            System.out.printf("L'enemic %s ja està mort\n", name);
            return;
        }
        resolveAtack(strength);

        System.out.printf("L'enemic %s té %d punts de vida després d'un atac de força %d\n", name, life, strength);
    }

    @Override
    public String toString() {
        return "Enemy{" +
                "name='" + name + '\'' +
                ", life=" + life +
                '}';
    }
}
