package cat.itb.jesuslopez07.dam.m03.uf4.generalexam.twitter;

public class Twitter {
    public static void main(String[] args) {
        Tweet tweet1 = new Tweet("iamdevloper", "07 de gener", "Remember, a few hours of trial and error can save you several minutes of looking at the README");
        Tweet tweet2 = new Tweet("softcatala", "29 de març", "Avui mateix, #CommonVoiceCAT segueix creixent \uD83D\uDE80:\n\uD83D\uDDE3️856 hores enregistrades\n✅ 725 de validades.\nSi encara no has participat, pots fer-ho aquí!");
        PollTweet pollTweet3 = new PollTweet("musicat", "02 d'abril", "Quina cançó t'agrada més?");
        pollTweet3.addChoice("Comèdia dramàtica - La Fúmiga");
        pollTweet3.addChoice("In the night - Oques Grasses");
        pollTweet3.addChoice("Una Lluna a l'Aigua - Txarango");
        pollTweet3.addChoice("Esbarzers - La Gossa Sorda");

        pollTweet3.vote(0);
        pollTweet3.vote(0);
        pollTweet3.vote(0);
        pollTweet3.vote(1);
        pollTweet3.vote(1);
        pollTweet3.vote(2);
        pollTweet3.vote(2);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        pollTweet3.vote(3);
        pollTweet3.vote(3);

        Tweet tweet4 = new Tweet("ProgrammerJokes", "05 d'abril", "Q: what's the object-oriented way to become weathy?\n\nA: Inheritance");

        tweet1.print();
        System.out.println();
        tweet2.print();
        System.out.println();
        pollTweet3.print();
        System.out.println();
        tweet4.print();
    }


}
