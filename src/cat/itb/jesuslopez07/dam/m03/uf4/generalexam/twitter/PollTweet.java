package cat.itb.jesuslopez07.dam.m03.uf4.generalexam.twitter;

import java.util.ArrayList;
import java.util.List;

public class PollTweet extends Tweet{
    private List<Choice> choices;
    private int totalVotes;

    public PollTweet(String user, String date, String text) {
        super(user, date, text);
        this.choices = new ArrayList<Choice>();
        totalVotes = 0;
    }

    public void addChoice(String text){
        choices.add(new Choice(text));
    }

    public void vote(int index){
        choices.get(index).vote();
        totalVotes++;
    }

    @Override
    public void print() {
        super.print();
        for(Choice c : choices){
            System.out.printf("- (%d/%d) %s\n", c.getVotes(), totalVotes, c.getText());
        }
    }
}
