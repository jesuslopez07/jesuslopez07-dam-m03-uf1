package cat.itb.jesuslopez07.dam.m03.uf4.generalexam.gameenemies;

public class Zombie extends Enemy{
    private String sound;

    public Zombie(String name, int life, String sound) {
        super(name, life);
        this.sound = sound;
    }

    @Override
    void resolveAtack(int strength) {
        System.out.println(sound);
        substractLife(strength);
    }
}
