package cat.itb.jesuslopez07.dam.m03.uf4.generalexam.gameenemies;

public class Troll extends Enemy{
    private int resistance;
    public Troll(String name, int life, int resistance) {
        super(name, life);
        this.resistance = resistance;
    }

    @Override
    void resolveAtack(int strength) {
        strength = Math.max(0, strength - resistance);
        substractLife(strength);
    }
}
