package cat.itb.jesuslopez07.dam.m03.uf4.generalexam.gameenemies;

public class Goblin extends Enemy{
    public Goblin(String name, int life) {
        super(name, life);
    }

    @Override
    void resolveAtack(int strength) {
        substractLife(strength < 5 ? 1 : 5);
    }
}
