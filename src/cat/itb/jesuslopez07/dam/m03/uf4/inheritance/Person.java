package cat.itb.jesuslopez07.dam.m03.uf4.inheritance;

public class Person {
    protected String name;
    protected int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return String.format("Person: %s (%d)", name, age);
    }
}
