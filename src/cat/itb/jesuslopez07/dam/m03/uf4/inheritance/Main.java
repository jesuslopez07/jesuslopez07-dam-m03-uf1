package cat.itb.jesuslopez07.dam.m03.uf4.inheritance;

public class Main {
    public static void main(String[] args) {
        Person p1 = new Person("Joan", 26);
        System.out.println(p1);

        Student s1 = new Student("Oriol", 20, 2020);
        System.out.println(s1);
    }
}
