package cat.itb.jesuslopez07.dam.m03.uf4.inheritance;

public class Student extends Person{
    private int startYear;

    public Student(String name, int age, int startYear){
        super(name, age);
        this.startYear = startYear;
    }

    @Override
    public String toString() {
        return String.format("Student (%d): %s", startYear, super.toString());
    }
}
