package cat.itb.jesuslopez07.dam.m03.uf5.practica;

public class Pais {
    // Atributos de Pais:
    private String nom;
    private String codi;
    private int casosNousConfirmats;
    private int totalConfirmats;
    private int novesMorts;
    private int mortsTotals;
    private int nousRecuperats;
    private int recuperatsTotals;

    //getters y setters:

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

    public int getCasosNousConfirmats() {
        return casosNousConfirmats;
    }

    public void setCasosNousConfirmats(int casosNousConfirmats) {
        this.casosNousConfirmats = casosNousConfirmats;
    }

    public int getTotalConfirmats() {
        return totalConfirmats;
    }

    public void setTotalConfirmats(int totalConfirmats) {
        this.totalConfirmats = totalConfirmats;
    }

    public int getNovesMorts() {
        return novesMorts;
    }

    public void setNovesMorts(int novesMorts) {
        this.novesMorts = novesMorts;
    }

    public int getMortsTotals() {
        return mortsTotals;
    }

    public void setMortsTotals(int mortsTotals) {
        this.mortsTotals = mortsTotals;
    }

    public int getNousRecuperats() {
        return nousRecuperats;
    }

    public void setNousRecuperats(int nousRecuperats) {
        this.nousRecuperats = nousRecuperats;
    }

    public int getRecuperatsTotals() {
        return recuperatsTotals;
    }

    public void setRecuperatsTotals(int recuperatsTotals) {
        this.recuperatsTotals = recuperatsTotals;
    }

    //toString de pais:
    @Override
    public String toString() {
        String dades =
                "\nNom:                   " + getNom() +
                "\nCodi:                  " + getCodi() +
                "\nCasos nous confirmats: " + getCasosNousConfirmats() +
                "\nTotal confirmat:       " + getTotalConfirmats() +
                "\nNoves morts:           " + getNovesMorts() +
                "\nMorts totals:          " + getMortsTotals() +
                "\nNous recuperats:       " + getNousRecuperats() +
                "\nRecuperats totals:     " + getRecuperatsTotals();
        return dades;
    }
}
