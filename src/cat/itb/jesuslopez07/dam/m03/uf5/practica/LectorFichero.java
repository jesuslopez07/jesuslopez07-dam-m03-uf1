package cat.itb.jesuslopez07.dam.m03.uf5.practica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class LectorFichero{

    //Función para leer el contenido del fichero, de los cuales nos
    // retornara el contenido en forma de lista de linias String.

    public ArrayList<String> readFileContent(String filePath){

        ArrayList<String> fileContent = new ArrayList<>();

        try{
            //Preparamos la lectura...
            File fichero = new File(filePath);
            BufferedReader br = new BufferedReader(new FileReader(fichero));

            //Guardamos cada linia del fichero en nuestra lista.
            String linea;
            while ((linea = br.readLine()) != null){
                fileContent.add(linea);
            }

            br.close();
        }catch (IOException e){
            e.printStackTrace();
        }

        return fileContent;
    }

    // Esta funcion sirve para leer el contenido de un fichero y guardarlo en una lista ID - VALOR
    // la variable separador es para definir un separador como tal, ya sea un espacio " " o un guión "-".
    // En este caso es un espacio por como esta hecho el fichero.
    public  HashMap<String, String> readFileContentIdValor(String filePath, String separador){

        HashMap<String, String> listaIdValor = new HashMap<>();

        //Creamos una lista nueva donde se guardaran las linias del fichero definido, para luego trabajar con ella...
        ArrayList<String> lineListFileContent = readFileContent(filePath);

        for (String linea : lineListFileContent) {

            //Separamos la linia en 2 valores y los guardamos en una lista.
            String[] split = linea.split(separador);

            //Guardamos los 2 valores como ID-Valor.
            listaIdValor.put(split[0], split[1]);
        }
        return  listaIdValor;
    }
}
