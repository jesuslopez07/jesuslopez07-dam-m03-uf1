package cat.itb.jesuslopez07.dam.m03.uf5.practica;

import java.text.DecimalFormat;
import java.util.*;
import java.util.stream.Stream;

public class CovidStatsApp {
    static LectorFichero lector;
    static ArrayList<Pais> listaPaises;
    static DecimalFormat formatDecimals = new DecimalFormat("#.##");
    public static void main(String[] args) {

        lector = new LectorFichero();

        int opcion;
        do{
            opcion = menu(); // Función que muestra un menu y pueda selccionar una opcion.

            switch (opcion){
                case 1: // Datos
                    datos();
                    break;
                case 2: // Totales
                    totales();
                    break;
                case 3: // Tops
                    tops();
                    break;
                case 4: // Top EU
                    topsEu();
                    break;
                case 5: // Spain by population
                    spainByPopulation();
                    break;
                case 6: // EU Top by population
                    euTopByPopulation();
                    break;
            }
        }while (opcion != 7); // Repetimos hasta que el usuario decida salir...
    }

    public static int menu(){
        // Opción seleccionada por el usuario.
        int opcion = -1;
        do{
            System.out.println("------------------------------");
            System.out.println("-       Covid Stats          -");
            System.out.println("------------------------------");
            System.out.println(" Selecciona una opció:");
            System.out.println("    1 - Todos los datos.");
            System.out.println("    2 - Totales.");
            System.out.println("    3 - Tops.");
            System.out.println("    4 - TOP EU.");
            System.out.println("    5 - Spain By Population.");
            System.out.println("    6 - Eu Tops by Population.");
            System.out.println("    7 - Salir.");
            System.out.println("");

            Scanner scanner = new Scanner(System.in);
            opcion = scanner.nextInt();
        }while (opcion < 1 || opcion > 7); // Repetimos la lista hasta que la opcion sea valida
        return opcion;
    }

    private static void datos(){
        ArrayList<String> listaContenidoFichero = lector.readFileContent("files/coviddata.txt");
        listaPaises = new ArrayList<>();
        //Como tenemos 8 datos diferentes para cada pais
        //(nom, codi, casos nous, casos totals, noves morts, morts totals, nous recuperats i recuperats totals)
        //Entonces cada 8 linias en nuestra lista es 1 pais.

        for (int i = 0; i < listaContenidoFichero.size(); i+=8){
            Pais pais = new Pais();
            pais.setNom(listaContenidoFichero.get(i + 0)); // Guardamos nombre.
            pais.setCodi(listaContenidoFichero.get(i + 1)); // Guardamos codigo.
            pais.setCasosNousConfirmats(Integer.parseInt(listaContenidoFichero.get(i + 2))); // Guardamos casos nuevos cconfirmados.
            pais.setTotalConfirmats(Integer.parseInt(listaContenidoFichero.get(i + 3))); // Guardamos casos totales confirmados.
            pais.setNovesMorts(Integer.parseInt(listaContenidoFichero.get(i + 4))); // Guardamos casos
            pais.setMortsTotals(Integer.parseInt(listaContenidoFichero.get(i + 5)));
            pais.setNousRecuperats(Integer.parseInt(listaContenidoFichero.get(i + 6)));
            pais.setRecuperatsTotals(Integer.parseInt(listaContenidoFichero.get(i + 7)));

            listaPaises.add(pais); // Lo añadimos a nuestra lista de paises.
        }

        for (Pais pais : listaPaises) {
            System.out.println(pais.toString()); // "Printamos" los paises...
        }
    }

    private static void totales(){
        // Primero comprobamos si hemos leido los datos de coviddata.txt
        if(listaPaises == null || listaPaises.size() == 0){
            System.out.println("Primero se tiene que seleccionar la opción 1.");
            return;
        }

        // Si nuestra lista de paises tiene dadas, significa que ya lo hemos leido previamente.
        // Y podemos calcular el total de cada categoria.

        // Varables para guardar las sumas totales de cada categoria...
        int sumaTotalCasosNuevosConfirmados = 0;
        int sumaTotalConfirmados = 0;
        int sumaTotalNuevasMuertes = 0;
        int sumaTotalMuertesTotales= 0;
        int sumaTotalNuevosRecuperados= 0;
        int sumaTotalRecuperadosTotal= 0;

        // Recorriendo la lista, sumaremos cada categoria para obtener los resultados totales.
        for (Pais pais : listaPaises) {
            sumaTotalCasosNuevosConfirmados += pais.getCasosNousConfirmats();
            sumaTotalConfirmados += pais.getTotalConfirmats();
            sumaTotalNuevasMuertes += pais.getNovesMorts();
            sumaTotalMuertesTotales += pais.getMortsTotals();
            sumaTotalNuevosRecuperados += pais.getNousRecuperats();
            sumaTotalRecuperadosTotal += pais.getRecuperatsTotals();
        }

        // Imprimimos por pantalla los totales que calculamos...
        System.out.println("### DADES TOTALS ###");
        System.out.println("Casos nous:       " + sumaTotalCasosNuevosConfirmados);
        System.out.println("Total confirmats: " + sumaTotalConfirmados);
        System.out.println("Noves morts:      " + sumaTotalNuevasMuertes);
        System.out.println("Morts totals:     " + sumaTotalMuertesTotales);
        System.out.println("Nous recuperats:  " + sumaTotalNuevosRecuperados);
        System.out.println("RecuperatsTotals: " + sumaTotalRecuperadosTotal);
        System.out.println();
    }

    private static void tops(){
        // Primero comprobamos si hemos leido los datos de coviddata.txt
        if(listaPaises == null || listaPaises.size() == 0){
            System.out.println("Primero se tiene que seleccionar la opción 1.");
            return;
        }

        // Variables para guardar el pais TOP de cada categoria.
        Pais topCasosNuevosConfirmados = Collections.max(listaPaises, Comparator.comparing(Pais::getCasosNousConfirmats).thenComparing(Pais::getNom));
        Pais topTotalConfirmados = Collections.max(listaPaises, Comparator.comparing(Pais::getTotalConfirmats).thenComparing(Pais::getNom));
        Pais topNuevasMuertes = Collections.max(listaPaises, Comparator.comparing(Pais::getNovesMorts).thenComparing(Pais::getNom));
        Pais topMuertesTotales = Collections.max(listaPaises, Comparator.comparing(Pais::getMortsTotals).thenComparing(Pais::getNom));
        Pais topNuevosRecuperados = Collections.max(listaPaises, Comparator.comparing(Pais::getNousRecuperats).thenComparing(Pais::getNom));
        Pais topRecuperadosTotales = Collections.max(listaPaises, Comparator.comparing(Pais::getRecuperatsTotals).thenComparing(Pais::getNom));

        // Ara que ja sabem el pais top de cada categoria els mostrem per pantalla
        System.out.println("### TOPS ###");
        System.out.println("País amb més casos nous: " + topCasosNuevosConfirmados.getNom());
        System.out.println("País amb més casos totals confirmats: " + topTotalConfirmados.getNom());
        System.out.println("País amb més noves morts: " + topNuevasMuertes.getNom());
        System.out.println("País amb més morts totals: " + topMuertesTotales.getNom());
        System.out.println("País amb més recuperats nous: " + topNuevosRecuperados.getNom());
        System.out.println("País amb més recuperats totals: " + topRecuperadosTotales.getNom());
        System.out.println();
    }

    private static void topsEu() {
        ArrayList<String> listaCodisEU = new ArrayList<>(Arrays.asList("BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL"));

        // Primero comprobamos que hayamos cargado primero el fichero coviddata.txt
        if (listaPaises == null || listaPaises.size() == 0) {
            System.out.println("Primer s'ha de llegir el fitxer de dades (1).");
            return;
        }

        // Creamos una nueva lista de Pais para meter los paises de EU
        ArrayList<Pais> listaPaisosEU = new ArrayList<>();
        for (Pais pais : listaPaises) {
            // Si la lista de codigos de paises de la EU tienen a dentro el codigo del pais actual
            // lo añadimos a la lista de listaPaisosEU
            if (listaCodisEU.contains(pais.getCodi())) {
                listaPaisosEU.add(pais);
            }
        }

        // Variables para guardar el pais TOP de cada categoria.
        Pais topCasosNuevosConfirmados = Collections.max(listaPaisosEU, Comparator.comparing(Pais::getCasosNousConfirmats).thenComparing(Pais::getNom));
        Pais topTotalConfirmados = Collections.max(listaPaisosEU, Comparator.comparing(Pais::getTotalConfirmats).thenComparing(Pais::getNom));
        Pais topNuevasMuertes = Collections.max(listaPaisosEU, Comparator.comparing(Pais::getNovesMorts).thenComparing(Pais::getNom));
        Pais topMuertesTotales = Collections.max(listaPaisosEU, Comparator.comparing(Pais::getMortsTotals).thenComparing(Pais::getNom));
        Pais topNuevosRecuperados = Collections.max(listaPaisosEU, Comparator.comparing(Pais::getNousRecuperats).thenComparing(Pais::getNom));
        Pais topRecuperadosTotales = Collections.max(listaPaisosEU, Comparator.comparing(Pais::getRecuperatsTotals).thenComparing(Pais::getNom));

        // Ara que ja sabem el pais top de cada categoria els mostrem per pantalla
        System.out.println("### TOPS EU###");
        System.out.println("País amb més casos nous: " + topCasosNuevosConfirmados.getNom());
        System.out.println("País amb més casos totals confirmats: " + topTotalConfirmados.getNom());
        System.out.println("País amb més noves morts: " + topNuevasMuertes.getNom());
        System.out.println("País amb més morts totals: " + topMuertesTotales.getNom());
        System.out.println("País amb més recuperats nous: " + topNuevosRecuperados.getNom());
        System.out.println("País amb més recuperats totals: " + topRecuperadosTotales.getNom());
        System.out.println();
    }

    private static void spainByPopulation() {

        // Primero comprobamos que hayamos cargado primero el fichero coviddata.txt
        if (listaPaises == null || listaPaises.size() == 0) {
            System.out.println("Primer s'ha de llegir el fitxer de dades (1).");
            return;
        }

        // Guardamos en una lista ID-VALOR los codigos de los paises
        HashMap<String, String> listaIdValorCodiPaisos = lector.readFileContentIdValor("files/country_codes.txt", " ");

        // Guardamos en una lista ID-VALOR la población total de cada pais
        HashMap<String, String> listaIdValorPoblacioPaisos = lector.readFileContentIdValor("files/population.txt", " ");

        String codiTresCifres = listaIdValorCodiPaisos.get("ES");

        int poblacio = Integer.parseInt(listaIdValorPoblacioPaisos.get(codiTresCifres));

        Pais espanya = new Pais();
        String codi = "";
        int i = 0;
        while (i < listaPaises.size() && codi != "ES") {

            Pais pais = listaPaises.get(i);
            if (pais.getCodi().equals("ES")) espanya = pais;

            i++;
            codi = pais.getCodi();
        }

        // Ahora que sabemos los datos, simplemente calculamos cada uno de ellos...
        double casosNousRelatius = (espanya.getCasosNousConfirmats() * 100f) / poblacio;
        double casosTotalsRelatius = (espanya.getTotalConfirmats() * 100f) / poblacio;
        double novesMortsRelatius = (espanya.getNovesMorts() * 100f) / poblacio;
        double mortsTotalsRelatius = (espanya.getMortsTotals() * 100f) / poblacio;
        double nousRecuperatsRelatius = (espanya.getNousRecuperats() * 100f) / poblacio;
        double recuperatsTotalsRelatius = (espanya.getRecuperatsTotals() * 100f) / poblacio;

        System.out.println("### Spain Relative ###");
        System.out.println("Casos nous:       " + formatDecimals.format(casosNousRelatius) + " %");
        System.out.println("Total confirmats: " + formatDecimals.format(casosTotalsRelatius) + " %");
        System.out.println("Noves morts:      " + formatDecimals.format(novesMortsRelatius) + " %");
        System.out.println("Morts totals:     " + formatDecimals.format(mortsTotalsRelatius) + " %");
        System.out.println("Nous recuperats:  " + formatDecimals.format(nousRecuperatsRelatius) + " %");
        System.out.println("RecuperatsTotals: " + formatDecimals.format(recuperatsTotalsRelatius) + " %");
        System.out.println();
    }

    private static void euTopByPopulation() {

        // Primero comprobamos que hayamos cargado primero el fichero coviddata.txt
        if (listaPaises == null || listaPaises.size() == 0) {
            System.out.println("Primer s'ha de llegir el fitxer de dades (1).");
            return;
        }

        ArrayList<String> listaCodisEU = new ArrayList<>(Arrays.asList("BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL"));

        ArrayList<Pais> listaPaisosEU = new ArrayList<>();
        for (Pais pais : listaPaises) {
            if (listaCodisEU.contains(pais.getCodi())) {
                listaPaisosEU.add(pais);
            }
        }

        HashMap<String, String> llistaIdValorCodiPaisos = lector.readFileContentIdValor("files/country_codes.txt", " ");

        HashMap<String, String> llistaIdValorPoblacioPaisos = lector.readFileContentIdValor("files/population.txt", " ");

        Pais paisCasosNousRelatius = new Pais();
        Pais paisCasosTotalsRelatius = new Pais();
        Pais paisNovesMortsRelatius = new Pais();
        Pais paisMortsTotalsRelatius = new Pais();
        Pais paisNousRecuperatsRelatius = new Pais();
        Pais paisRecuperatsTotalsRelatius = new Pais();
        double topCasosNousRelatius = 0;
        double topCasosTotalsRelatius = 0;
        double topNovesMortsRelatius = 0;
        double topMortsTotalsRelatius = 0;
        double topNousRecuperatsRelatius = 0;
        double topRecuperatsTotalsRelatius = 0;

        for (Pais paisEU : listaPaisosEU) {

            String codiDuesCifres = paisEU.getCodi();
            String codiTresCifres = llistaIdValorCodiPaisos.get(codiDuesCifres);

            int poblacio = Integer.parseInt(llistaIdValorPoblacioPaisos.get(codiTresCifres));


            double casosNousRelatius = (paisEU.getCasosNousConfirmats() * 100f) / poblacio;
            double casosTotalsRelatius = (paisEU.getTotalConfirmats() * 100f) / poblacio;
            double novesMortsRelatius = (paisEU.getNovesMorts() * 100f) / poblacio;
            double mortsTotalsRelatius = (paisEU.getMortsTotals() * 100f) / poblacio;
            double nousRecuperatsRelatius = (paisEU.getNousRecuperats() * 100f) / poblacio;
            double recuperatsTotalsRelatius = (paisEU.getRecuperatsTotals() * 100f) / poblacio;


            if (topCasosNousRelatius < casosNousRelatius) {
                topCasosNousRelatius = casosNousRelatius;
                paisCasosNousRelatius = paisEU;
            }
            if (topCasosTotalsRelatius < casosTotalsRelatius) {
                topCasosTotalsRelatius = casosTotalsRelatius;
                paisCasosTotalsRelatius = paisEU;
            }
            if (topNovesMortsRelatius < novesMortsRelatius) {
                topNovesMortsRelatius = novesMortsRelatius;
                paisNovesMortsRelatius = paisEU;
            }
            if (topMortsTotalsRelatius < mortsTotalsRelatius) {
                topMortsTotalsRelatius = mortsTotalsRelatius;
                paisMortsTotalsRelatius = paisEU;
            }
            if (topNousRecuperatsRelatius < nousRecuperatsRelatius) {
                topNousRecuperatsRelatius = nousRecuperatsRelatius;
                paisNousRecuperatsRelatius = paisEU;
            }
            if (topRecuperatsTotalsRelatius < recuperatsTotalsRelatius) {
                topRecuperatsTotalsRelatius = recuperatsTotalsRelatius;
                paisRecuperatsTotalsRelatius = paisEU;
            }
        }

        // Ahora imprimimos por pantalla el resultado...
        System.out.println("### Spain Relative ###");
        System.out.println("Casos nous:       " + formatDecimals.format(topCasosNousRelatius) + " % (" + paisCasosNousRelatius.getNom() + ")");
        System.out.println("Total confirmats: " + formatDecimals.format(topCasosTotalsRelatius) + " % (" + paisCasosTotalsRelatius.getNom() + ")");
        System.out.println("Noves morts:      " + formatDecimals.format(topNovesMortsRelatius) + " % (" + paisNovesMortsRelatius.getNom() + ")");
        System.out.println("Morts totals:     " + formatDecimals.format(topMortsTotalsRelatius) + " % (" + paisMortsTotalsRelatius.getNom() + ")");
        System.out.println("Nous recuperats:  " + formatDecimals.format(topNousRecuperatsRelatius) + " % (" + paisNousRecuperatsRelatius.getNom() + ")");
        System.out.println("RecuperatsTotals: " + formatDecimals.format(topRecuperatsTotalsRelatius) + " % (" + paisRecuperatsTotalsRelatius.getNom() + ")");
        System.out.println();
    }
}
