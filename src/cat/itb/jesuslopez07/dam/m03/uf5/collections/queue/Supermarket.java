package cat.itb.jesuslopez07.dam.m03.uf5.collections.queue;

import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

public class Supermarket {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Queue<String> queue = new LinkedList<String>();
        while (true){
            String name = scanner.next();

            if(name.equals("END"))
                break;
            else if(name.equals("NEXT")){
                String next = queue.poll();
                if(next == null)
                    System.out.println("No hi ha ningú a la cua");
                else
                    System.out.printf("Següent: %s\n", next);
            }else{
                queue.add(name);
            }
            System.out.println(queue);
        }
    }

}
