package cat.itb.jesuslopez07.dam.m03.uf5.collections.stack;

import java.util.Locale;
import java.util.Scanner;
import java.util.Stack;

public class Cards {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Stack<String> stack = new Stack<String>();

        while (true){
            String name = scanner.nextLine();

            if(name.equals("END"))
                break;

            else if(name.equals("NEXT")){
                if(stack.empty())
                    System.out.println("No hi queden cartes.");
                else {
                    String next = stack.pop();
                    System.out.printf("Següent carta: %s\n", next);
                }
            }else{
                stack.push(name);
            }
            System.out.println(stack);
        }
    }

}
