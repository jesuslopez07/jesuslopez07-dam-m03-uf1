package cat.itb.jesuslopez07.dam.m03.uf5.collections.queue;

import java.util.Comparator;
import java.util.Locale;
import java.util.PriorityQueue;
import java.util.Scanner;

public class HospitalQueue {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Comparator<Client> comparator = (p1, p2) -> p2.getProductCount() - p1.getProductCount();
        PriorityQueue<Client> queue = new PriorityQueue<>(comparator);
        while (true){
            int n = scanner.nextInt();

            if(n == -1){
                break;
            }
            else if(n == 1){
                String name = scanner.next();
                int prioridad = scanner.nextInt();
                queue.add(new Client(name, prioridad));
            }
        }
    }
}
