package cat.itb.jesuslopez07.dam.m03.uf5.collections.map;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class NameCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Map<String, Integer> contador = new HashMap<String, Integer>();
        System.out.println(contador);

        while(true){
            String name = scanner.nextLine();
            if(name.equals(""))
                break;

            if(contador.containsKey(name)){
                int current = contador.get(name);
                contador.put(name, current + 1);
            }else {
                contador.put(name, 1);
            }
            System.out.println(contador);
            for(String clau : contador.keySet())
                System.out.println(clau);
        }
    }

}
