package cat.itb.jesuslopez07.dam.m03.uf5.collections.map.employee;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class EmployeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Map<String, Employee> employees = new HashMap<String, Employee>();

        int numberEmployee = scanner.nextInt();
        scanner.nextLine();

        for(int i = 0; i < numberEmployee; i++){
            Employee e = Employee.readEmployee(scanner);
            String id = e.getId();
            employees.put(id, e);
        }

        while(true){
            String id = scanner.next();
            if(id.equals("END"))
                break;

            if(employees.containsKey(id)){
                System.out.println(employees.get(id));
            }else{
                System.out.println("Aquest empleat no existeix.");
            }
        }
    }

}
