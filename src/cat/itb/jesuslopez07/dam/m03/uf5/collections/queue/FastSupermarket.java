package cat.itb.jesuslopez07.dam.m03.uf5.collections.queue;

import java.util.Comparator;
import java.util.Locale;
import java.util.PriorityQueue;
import java.util.Scanner;

public class FastSupermarket {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Comparator<Client> comparator = (c1, c2) -> c2.getProductCount() - c1.getProductCount();
        PriorityQueue<Client> queue = new PriorityQueue<>(comparator);
        while (true){
            String name = scanner.next();

            if(name.equals("END"))
                break;
            else if(name.equals("NEXT")){
                Client next = queue.poll();
                if(next == null)
                    System.out.println("No hi ha ningú a la cua");
                else
                    System.out.printf("Següent: %s\n", next);
            }else{
                int productCount = scanner.nextInt();
                queue.add(new Client(name, productCount));
            }
            System.out.println(queue);
        }
    }

}
