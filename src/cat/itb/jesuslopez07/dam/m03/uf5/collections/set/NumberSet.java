package cat.itb.jesuslopez07.dam.m03.uf5.collections.set;

import java.util.HashSet;
import java.util.Set;

public class NumberSet {
    public static void main(String[] args) {
        Set<Integer> parells = new HashSet<>();
        for(int i = -10; i <= 10; i+=2)
            parells.add(i);
        Set<Integer> positius = new HashSet<>();
        for(int i = 0; i <= 10; i++)
            positius.add(i);

        System.out.println("Parells: " + parells);
        System.out.println("Positius: " + positius);

        Set<Integer> interseccio = new HashSet<Integer>(parells);
        interseccio.retainAll(positius);
        System.out.println("Interseccio:" + interseccio);

        Set<Integer> unio = new HashSet<Integer>(parells);
        unio.addAll(positius);
        System.out.println("Unio:" + unio);

        Set<Integer> senars = new HashSet<Integer>(positius);
        senars.removeAll(parells);
        System.out.println("Senars:" + senars);
    }

}
