package cat.itb.jesuslopez07.dam.m03.uf5.collections.map;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class RoadSigns {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        Map<Integer,String> signs = new HashMap<Integer,String>();
        int numberSign = scanner.nextInt();

        for(int i = 0; i < numberSign; i++){
            int km = scanner.nextInt();
            String signName = scanner.next();;
            signs.put(km, signName);
        }

        while(true){
            int km = scanner.nextInt();
            if(km == -1)
                break;

            if(signs.containsKey(km)){
                System.out.println(signs.get(km));
            } else {
                System.out.println("no hi ha cartell");
            }
        }
    }

}
