package cat.itb.jesuslopez07.dam.m03.uf5.collections.queue;

public class Client implements Comparable<Client>{
    private String name;
    private int productCount;

    public Client(String name, int productCount) {
        this.name = name;
        this.productCount = productCount;
    }

    public String getName() {
        return name;
    }

    public int getProductCount() {
        return productCount;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", productCount=" + productCount +
                '}';
    }

    @Override
    public int compareTo(Client client) {
        return this.productCount - client.getProductCount();
    }
}
