package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import cat.itb.jesuslopez07.dam.m03.uf4.abstracts.figure.*;
import cat.itb.jesuslopez07.dam.m03.uf4.exercises.figures.ConsoleColors;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class FigurePainterNotGreen {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Figure> figures = new ArrayList<>();

        figures.add(new RectangleFigure(4, 5, ConsoleColors.RED));
        figures.add(new TriangleFigure(3, ConsoleColors.YELLOW));
        figures.add(new RectangleFigure(3, 5, ConsoleColors.GREEN));
        figures.add(new RectangleFigure(1, 1, ConsoleColors.BLUE));
        figures.add(new TriangleFigure(4, ConsoleColors.YELLOW));
        figures.add(new RectangleFigure(10, 5, ConsoleColors.GREEN));
        figures.add(new TriangleFigure(8, ConsoleColors.YELLOW));

        figures.stream()
                .filter(x -> !x.getColor().equals(ConsoleColors.GREEN))
                .forEach(Figure::paint);
    }
}
