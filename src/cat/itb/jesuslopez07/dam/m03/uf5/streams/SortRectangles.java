package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import cat.itb.jesuslopez07.dam.m03.uf2.classfun.Rectangle;

import java.util.*;

public class SortRectangles {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Rectangle> rectangles = new ArrayList<>();

        int n = scanner.nextInt();
        for(int i = 0; i < n; i++){
            rectangles.add(Rectangle.readRectangle(scanner));
        }
        System.out.println(rectangles);
        rectangles.sort(Comparator.comparing(Rectangle::getArea).reversed());
        System.out.println(rectangles);
        rectangles.forEach(r -> System.out.println(r.getArea()));
        Rectangle maxArea = Collections.max(rectangles, Comparator.comparing(Rectangle::getPerimetro));
        System.out.println(maxArea);
    }
}
