package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import java.util.*;

public class SortByLastDigit {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = Arrays.asList(253,432,65,234,43,16,28,432,34,65,312,34,2134,76,2,76,23,67,27,8,54235256,4560,7431);
        System.out.println(list);
        list.sort(
                Comparator.comparingInt(x -> (int)x % 10)
                .thenComparingInt(x -> (int)x)
        );
        System.out.println(list);
    }
}
