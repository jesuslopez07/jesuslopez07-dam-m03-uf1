package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class LamdaSample {
    public static boolean endsWithThree(int n){
        return n % 10 == 3;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> numbers = IntegerLists.readIntegerList(scanner);
        System.out.println(numbers);

        numbers.removeIf(LamdaSample::endsWithThree);
        System.out.println(numbers);

        numbers.sort((v1, v2) -> v2-v1);
        System.out.println(numbers);

        numbers.forEach(System.out::println);
    }
}
