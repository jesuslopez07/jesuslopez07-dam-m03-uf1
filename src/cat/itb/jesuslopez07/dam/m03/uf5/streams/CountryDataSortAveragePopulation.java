package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class CountryDataSortAveragePopulation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Country> countries = new ArrayList<>();

        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            countries.add(Country.readCountry(scanner));
        }

        double average = countries.stream()
                .filter(x -> x.getSuperficie() < 1200000)
                .mapToDouble(x -> x.getDensitat() * x.getSuperficie())
                .average().getAsDouble();
        System.out.printf("Mitjana: %.2f\n", average);
    }
}
