package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import java.util.Scanner;

public class Country {
    private String nom;
    private String capital;
    private int superficie;
    private int densitat;

    public Country(String nom, String capital, int superficie, int densitat) {
        this.nom = nom;
        this.capital = capital;
        this.superficie = superficie;
        this.densitat = densitat;
    }

    public static Country readCountry(Scanner scanner) {
        String nom = scanner.next();
        String capital = scanner.next();
        int superficie = scanner.nextInt();
        int densitat = scanner.nextInt();
        return new Country(nom, capital, superficie, densitat);
    }

    public String getNom() {
        return nom;
    }

    public String getCapital() {
        return capital;
    }

    public int getSuperficie() {
        return superficie;
    }

    public int getDensitat() {
        return densitat;
    }

    public long getHabitants(){
        return (long) densitat * superficie;
    }

    @Override
    public String toString() {
        return "Country{" +
                "nom='" + nom + '\'' +
                ", capital='" + capital + '\'' +
                ", superficie=" + superficie +
                ", densitat=" + densitat +
                '}';
    }
}
