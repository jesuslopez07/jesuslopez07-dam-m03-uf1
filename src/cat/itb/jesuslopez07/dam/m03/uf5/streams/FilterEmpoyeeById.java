package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import cat.itb.jesuslopez07.dam.m03.uf5.collections.map.employee.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class FilterEmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Employee> employees = new ArrayList<>();
        int n = scanner.nextInt();
        scanner.nextLine();

        for (int i =0; i < n; i++)
            employees.add(Employee.readEmployee(scanner));

        employees.removeIf(x -> x.getId().endsWith("A"));
        System.out.println(employees);

    }
}
