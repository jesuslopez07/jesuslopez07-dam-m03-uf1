package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import cat.itb.jesuslopez07.dam.m03.uf2.dataclasses.Treballador;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class FromAvgSallaryInfoStream {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nTreballadors = scanner.nextInt();
        List<Treballador> treballadors = new ArrayList<>();

        for (int i = 0; i < nTreballadors; i++) {
            int sou = scanner.nextInt();
            String nom = scanner.next();
            Treballador t = new Treballador(nom, sou);
            treballadors.add(t);
        }

        double average = treballadors.stream()
                .mapToDouble(Treballador::getSou)
                .average().getAsDouble();

        treballadors.stream()
                .filter(x -> x.getSou() < average)
                .forEach(System.out::println);
    }
}
