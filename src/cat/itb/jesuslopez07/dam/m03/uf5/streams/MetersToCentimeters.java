package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class MetersToCentimeters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        list.stream().map(x -> x * 100).forEach(System.out::println);
    }
}
