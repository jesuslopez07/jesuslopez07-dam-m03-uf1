package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import cat.itb.jesuslopez07.dam.m03.uf2.dataclasses.Product;

import java.util.*;

public class MostExpensiveProduct {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nombreProductes = scanner.nextInt();
        List<Product> productes = new ArrayList<>();

        for (int i = 0; i < nombreProductes; i++) {
            String nom = scanner.next();
            double preu = scanner.nextDouble();
            productes.add(new Product(nom, preu));
        }

        for (Product p : productes){
            System.out.printf("El producte %s val %.2f€\n", p.getName(), p.getPrize());
        }

        Product maxim = Collections.max(productes, (p1, p2) -> Double.compare(p1.getPrize(), p2.getPrize()));
        System.out.println("Màxim: " + maxim);
        
        Product maxim2 = Collections.max(productes, Comparator.comparing(p -> p.getPrize()));
        System.out.println("Màxim: " + maxim2);

        Product maxim3 = Collections.max(productes, Comparator.comparing(Product::getPrize));
        System.out.println("Màxim: " + maxim3);
    }
}
