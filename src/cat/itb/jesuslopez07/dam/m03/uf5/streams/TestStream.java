package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestStream {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Integer> list = Arrays.asList(4, 1, 2, 2, -1, 4, 5, 5, 0, 7, 8, 9, 10);
        System.out.println(list);
        long count = list.stream().filter(x -> x % 2 == 0).distinct().count();
        System.out.println(count);
        List<Integer> doble = list.stream().map(x -> x * 2).collect(Collectors.toList());
        System.out.println(doble);
    }
}
