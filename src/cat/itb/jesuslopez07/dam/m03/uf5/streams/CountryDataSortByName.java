package cat.itb.jesuslopez07.dam.m03.uf5.streams;

import java.util.*;

public class CountryDataSortByName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        List<Country> countries = new ArrayList<>();

        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            countries.add(Country.readCountry(scanner));
        }

        countries.stream().filter(x -> x.getDensitat() > 5)
                .sorted(Comparator.comparing(Country::getNom, String.CASE_INSENSITIVE_ORDER))
                .forEach(System.out::println);
    }
}
