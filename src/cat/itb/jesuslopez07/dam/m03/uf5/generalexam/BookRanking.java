package cat.itb.jesuslopez07.dam.m03.uf5.generalexam;

import java.util.*;

public class BookRanking {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        scanner.nextLine();

        List<Book> books = new ArrayList<>();
        for (int i = 0; i < n; i++){
            Book book = Book.readBook(scanner);
            books.add(book);
        }

        // Llibre amb més pagines
        System.out.println("### Llibre amb més pàgines");
        Book max = Collections.max(books, Comparator.comparing(Book::getPages));
        System.out.println(max);
        System.out.println();

        // Llibres per any
        System.out.println("### Llibres per any");
        books.stream()
                .sorted(Comparator.comparing(Book::getYear).reversed()
                        .thenComparing(Book::getTitle))
                .forEach(System.out::println);
        System.out.println();

        // mitjana de pàgines
        System.out.println("### Mitjana de pàgines");
        double mitjana = books.stream()
                .mapToDouble(Book::getPages).average().getAsDouble();
        System.out.printf("%.1f\n", mitjana);
        System.out.println();

        // Llibres publicats després del 2018
        System.out.println("### Llibres publicats després del 2018");
        books.stream().filter(x -> x.getYear() > 2018)
                .forEach(System.out::println);
        System.out.println();

        // Suma de anys per pàgines
        System.out.println("### Suma de anys per pàgines");
        int suma = books.stream()
                .mapToInt(x -> x.getPages() * x.getYear()).sum();
        System.out.println(suma);
        System.out.println();

        System.out.println("### Més de 100 pàgines");
        books.stream()
                .filter(x -> x.getPages() > 100)
                .sorted(Comparator.comparing(Book::getTitle))
                .map(Book::getTitle)
                .forEach(System.out::println);
    }
}
