package cat.itb.jesuslopez07.dam.m03.uf5.generalexam;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class SearchByISBN {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        scanner.nextLine();

        Map<String, Book> books = new HashMap<>();
        for (int i = 0; i < n; i++){
            Book book = Book.readBook(scanner);
            books.put(book.getIsbn(), book);
        }

        int nISBN = scanner.nextInt();
        for (int i = 0; i < nISBN; i++){
            String isbn = scanner.next();
            if(books.containsKey(isbn))
                System.out.println(books.get(isbn));
            else
                System.out.println("Not found");
        }
    }
}
