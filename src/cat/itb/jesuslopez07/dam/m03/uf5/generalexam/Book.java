package cat.itb.jesuslopez07.dam.m03.uf5.generalexam;

import java.util.Scanner;

public class Book {
    private String title;
    private String author;
    private String isbn;
    private int pages;
    private int year;

    public Book(String title, String author, String isbn, int pages, int year) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
        this.pages = pages;
        this.year = year;
    }

    public static Book readBook(Scanner scanner){
        String title = scanner.nextLine();
        System.out.println(title);
        String author = scanner.nextLine();
        System.out.println(author);
        String isbn = scanner.nextLine();
        System.out.println(isbn);
        int pages = scanner.nextInt();
        System.out.println(pages);
        int year = scanner.nextInt();
        System.out.println(year);
        scanner.nextLine();
        return new Book(title, author, isbn, pages, year);
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }

    public int getPages() {
        return pages;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return String.format("%s - %s - %d", title, author, year);
    }
}
