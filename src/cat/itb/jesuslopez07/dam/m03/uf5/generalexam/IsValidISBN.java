package cat.itb.jesuslopez07.dam.m03.uf5.generalexam;

import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IsValidISBN {
    public static boolean isValidISBN(String isbn){
        Pattern isbnPattern = Pattern.compile("(978)?\\d{9}-[\\dX]");
        Matcher matcher = isbnPattern.matcher(isbn);
        return matcher.matches();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++){
            String isbn = scanner.next();
            System.out.println(isValidISBN(isbn));
        }
    }
}
