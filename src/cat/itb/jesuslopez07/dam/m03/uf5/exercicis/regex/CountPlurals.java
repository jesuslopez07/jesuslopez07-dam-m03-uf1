package cat.itb.jesuslopez07.dam.m03.uf5.exercicis.regex;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountPlurals {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Pattern wordPattern = Pattern.compile("(\\w*s)\\b");

        String text = scanner.nextLine();
        Matcher matcher = wordPattern.matcher(text);
        int count = 0;
        while(matcher.find()){
            String paraula = matcher.group(1);
            if(paraula.endsWith("s")){
                count++;
            }
        }
        System.out.println(count);
    }
}
