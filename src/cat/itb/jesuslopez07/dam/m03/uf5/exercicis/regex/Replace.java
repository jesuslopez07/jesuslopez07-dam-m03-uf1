package cat.itb.jesuslopez07.dam.m03.uf5.exercicis.regex;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Replace {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        Pattern emailPattern = Pattern.compile("(\\w+(?:\\.\\w+)*)@(\\w+(?:\\.\\w+)+)");
        while(true){
            String email = scanner.next();
            if(email.equals("")){
                break;
            }
            Matcher matcher = emailPattern.matcher(email);
            while(matcher.find()){
                int groupNum = scanner.nextInt();
                String group = matcher.group(groupNum);
                System.out.println(group);
            }
        }
    }
}
