package cat.itb.jesuslopez07.dam.m03.uf5.exercicis.regex;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadSubjectInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        Pattern subjectPattern = Pattern.compile("(\\p{Upper}+)-M(\\d+)UF(\\d+)");
        int num = scanner.nextInt();
        for (int i = 0; i < num; i++) {
            String subject = scanner.next();
            Matcher matcher = subjectPattern.matcher(subject);
            while (matcher.find()) {
                String cicle = matcher.group(1);
                int numModul = Integer.parseInt(matcher.group(2));
                int numUF = Integer.parseInt(matcher.group(3));
                System.out.printf("Estàs cursant la unitat formativa %d, del mòdul %d de %s.", numUF, numModul, cicle);
            }
        }
    }
}
