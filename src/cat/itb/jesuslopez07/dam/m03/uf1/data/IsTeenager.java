package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class IsTeenager {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int edat = scanner.nextInt();

        boolean isTeenager = edat >= 10 && edat <= 20;

        System.out.println(isTeenager);

    }
}
