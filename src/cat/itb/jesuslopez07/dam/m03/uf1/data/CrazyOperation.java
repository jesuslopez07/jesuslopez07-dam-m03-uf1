package cat.itb.jesuslopez07.dam.m03.uf1.data;
/*
 * L'usuari escriu 4 enters i s'imprimeix el valor de suma el primer amb el segon, mutiplicat per la resta del tercer amb quart
 */
public class CrazyOperation {
    public static void  main(String[] args) {
        int PrimerNumEnter = 5;
        int SegonNumEnter = 13;
        int TercerNumEnter = 9;
        int QuartNumEnter = 3;
        int ResultatTotal = (PrimerNumEnter + SegonNumEnter) * (TercerNumEnter - QuartNumEnter);
        System.out.println(ResultatTotal);
    }
}
