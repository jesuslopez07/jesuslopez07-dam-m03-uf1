package cat.itb.jesuslopez07.dam.m03.uf1.data;
/*
 * Calcula l'area d'un rectangle (l'usuari introdueix la llargada dels dos costats en valor enter)
 */

import java.util.Locale;
import java.util.Scanner;

public class IntRectangleArea {
    public static void  main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int base = scanner.nextInt();
        int altura = scanner.nextInt();
        int totalArea = base * altura;
        System.out.println(totalArea + "cm");
    }
}
