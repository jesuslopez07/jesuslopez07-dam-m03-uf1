package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class NextNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n = scanner.nextInt();
        int nextNumber = n + 1;

        System.out.printf("Després ve el %d", nextNumber);
    }
}
