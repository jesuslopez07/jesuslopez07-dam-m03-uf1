package cat.itb.jesuslopez07.dam.m03.uf1.data;
/*
 * L'usuari escriu un valor amb decimals i s'imprimeix el doble
 */
public class DoubleDoubleMe {
    public static void main(String[] args) {
        double FirstNum = 3.7;
        double TotalNum = FirstNum * 2;
        System.out.println(TotalNum);
    }
}
