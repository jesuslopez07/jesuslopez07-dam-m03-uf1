package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Scanner;
import java.util.Locale;

public class DivideBill {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nombreComençals = scanner.nextInt();
        double preuSopar = scanner.nextDouble();

        double preuPerPersona = preuSopar / nombreComençals;

        System.out.println(preuPerPersona);
    }
}

