package cat.itb.jesuslopez07.dam.m03.uf1.data.practica;

import java.util.Locale;
import java.util.Scanner;

public class AverageSpeed {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in).useLocale(Locale.US);
        double distanciaRecorreguda= scanner.nextDouble();
        double temps= scanner.nextDouble();

        double tempsHora= temps/60;

        double velocitatMitjana= distanciaRecorreguda/tempsHora;

        System.out.println(velocitatMitjana+"km/h");
    }
}
