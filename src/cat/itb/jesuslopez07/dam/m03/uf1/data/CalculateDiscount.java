package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

/*
 * L'usuari entra un preu original i el preu actual i s'imprimeix el descompte (en %)
 */
public class CalculateDiscount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        double PreuOriginal = scanner.nextDouble();
        double PreuActual = scanner.nextDouble();
        double Descompte = 100 - (100 * PreuActual / PreuOriginal);
        System.out.println(Descompte + "%");
    }
}
