package cat.itb.jesuslopez07.dam.m03.uf1.data.practica;

import java.util.Locale;
import java.util.Scanner;

public class IsOdd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int numqualsevol = scanner.nextInt();
        int num = numqualsevol%2;

        boolean numresultant = num != 0;
        System.out.println(numresultant);
    }
}
