package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class IsLegalAge {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int edat = scanner.nextInt();

        boolean edatLegal = edat == 18;
        System.out.println(edatLegal);
    }
}
