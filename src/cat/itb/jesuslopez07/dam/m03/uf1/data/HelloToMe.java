package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class HelloToMe {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        String name = scanner.next();

        System.out.println("Bon dia "+ name);
    }
}
