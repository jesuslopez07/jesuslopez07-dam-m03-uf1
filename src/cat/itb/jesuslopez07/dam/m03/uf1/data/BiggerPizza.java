package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Scanner;
import java.util.Locale;

public class BiggerPizza {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        double diametre = scanner.nextDouble();
        double superficieRodona = Math.PI * Math.pow(diametre/2, 2);
        System.out.printf("Superficie rodona: %.2f (%.2f de radi)\n", superficieRodona, diametre/2);

        double base = scanner.nextDouble();
        double altura = scanner.nextDouble();
        double superficieRectangular = base * altura;
        System.out.printf("Superficie rectanguar: %.2f (%.2f * %.2f)\n", superficieRectangular, base, altura);

        boolean biggerPizza = superficieRodona > superficieRectangular;
        System.out.println(biggerPizza);
    }
}

