package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class HowBigIsMyPizza {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        double diametre = scanner.nextInt();
        double superficie = (Math.PI * Math.pow(diametre, 2));
        System.out.printf("La superficie de la pizza és: %.0f u.\n", superficie);

    }
}
