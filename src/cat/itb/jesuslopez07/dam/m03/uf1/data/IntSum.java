package cat.itb.jesuslopez07.dam.m03.uf1.data;

/**
 * L'usuari escriu dos enters i s'imprimeix la suma dels dos.
 */

public class IntSum {
    public static void  main(String[] args) {
        int primerNum = 4;
        int segonNum = 5;
        int totalNum = primerNum + segonNum;
        System.out.println(totalNum);
    }
}
