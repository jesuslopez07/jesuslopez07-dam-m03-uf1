package cat.itb.jesuslopez07.dam.m03.uf1.data.practica;

import java.util.Locale;
import java.util.Scanner;

public class NiceDivide {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();

        int resultatDivisio = num1 / num2;
        int modul = num1 % num2;

        System.out.println(num1 + " dividit entre " + num2 + " és " + resultatDivisio + " mòdul " + modul);
    }
}
