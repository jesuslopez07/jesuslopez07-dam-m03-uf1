package cat.itb.jesuslopez07.dam.m03.uf1.data;

import java.util.Locale;
import java.util.Scanner;

public class IsGreater {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n1 = scanner.nextInt();
        int n2 = scanner.nextInt();

        boolean IsGreater = n1 > n2;

        System.out.println(IsGreater);
    }
}
