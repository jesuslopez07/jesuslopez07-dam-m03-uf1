package cat.itb.jesuslopez07.dam.m03.uf1.data;

/**
 * L'usuari escriu un enters i s'imprimeix el doble del valor entrat.
 */

public class IntDoubleMe {
    public static void main(String[] args) {
        int primerNum = 3;
        int totalNum = primerNum * 2;
        System.out.println(totalNum);
    }
}
