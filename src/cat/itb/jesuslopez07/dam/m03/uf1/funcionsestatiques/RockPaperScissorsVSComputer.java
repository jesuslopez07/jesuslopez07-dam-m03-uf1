package cat.itb.jesuslopez07.dam.m03.uf1.funcionsestatiques;

import java.util.Scanner;

public class RockPaperScissorsVSComputer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();
        double random = Math.random() * (4 - 1) + 1;
        double valor2 = Math.floor(random);
        System.out.println(valor2);
        boolean guanyaPrimer = (valor1 == 1 && valor2 == 3)
                || (valor1 == 2 && valor2 == 1)
                || (valor1 == 3 && valor2 == 2);
        if (valor1 == valor2)
            System.out.println("Empat");
        else if (guanyaPrimer)
            System.out.println("Guanya primer");
        else
            System.out.println("Guanya segon");
    }

}
