package cat.itb.jesuslopez07.dam.m03.uf1.funcionsestatiques;

import java.util.Scanner;

public class HiddenNumber {
    public static void main(String[] args) {
        int min = 1;
        int max = 4;
        double pensat = Math.random() * (max - min) + min;
        double pensat2 = Math.floor(pensat);
        System.out.println("Quin numero he pensat?");
        Scanner scanner = new Scanner(System.in);
        int elegit = scanner.nextInt();
        boolean acertat = pensat2 == elegit;
        System.out.println(acertat);

    }

}
