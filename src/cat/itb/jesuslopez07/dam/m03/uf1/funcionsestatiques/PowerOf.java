package cat.itb.jesuslopez07.dam.m03.uf1.funcionsestatiques;

import java.util.Locale;
import java.util.Scanner;

public class PowerOf {
    static public double potenciaBaseExponent(double base, double exponent) {
        return Math.pow(base, exponent);
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        double base = scanner.nextInt();
        double exponent = scanner.nextInt();
        double valorTotal= potenciaBaseExponent(base, exponent);

        System.out.println(base+" elevat a "+exponent+" és igual a "+valorTotal);
    }
}
