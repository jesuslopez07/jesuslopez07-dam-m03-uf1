package cat.itb.jesuslopez07.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class StoreFirstOnly {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> valors = new ArrayList<Integer>();

        int numActual = scanner.nextInt();

        while (numActual != -1){
            valors.add(numActual);
            numActual = scanner.nextInt();
        }

        numActual = scanner.nextInt();

        for (int valor : valors){
            if(numActual == valor){

            }
        }

        System.out.println(valors);
    }
}
