package cat.itb.jesuslopez07.dam.m03.uf1.arraysdin;

import java.util.*;

public class StrangeOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<Integer> valors = new ArrayList<Integer>();

        int numActual = scanner.nextInt();

        while (numActual != -1){
            if (valors.size() % 2 == 0){
                valors.add(0, numActual);
            }else
                valors.add(numActual);
            numActual = scanner.nextInt();
        }
        System.out.println(valors);
    }
}
