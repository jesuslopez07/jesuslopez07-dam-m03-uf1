package cat.itb.jesuslopez07.dam.m03.uf1.arraysdin;

import java.util.*;

public class PassaLlista {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        List<String> alumnes = new ArrayList<String>(Arrays.asList("Magdalena",
                "Magí",
                "Manel",
                "Manela",
                "Manuel",
                "Manuela",
                "Mar",
                "Marc",
                "Margalida",
                "Marçal",
                "Marcel",
                "Maria",
                "Maricel",
                "Marina",
                "Marta",
                "Martí",
                "Martina"));
        System.out.println(alumnes);

        int numActual = scanner.nextInt();

        while (numActual != -1){
          alumnes.remove(numActual);
          numActual = scanner.nextInt();
        }System.out.println(alumnes);

    }
}
