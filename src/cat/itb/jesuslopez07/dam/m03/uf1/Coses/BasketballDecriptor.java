package cat.itb.jesuslopez07.dam.m03.uf1.Coses;

import java.util.ArrayList;
import java.util.Scanner;

public class BasketballDecriptor {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        /*
        Más de lo mismo, pedimos puntuaciones hasta que introduzca "-1".
        Las puntuaciones las vamos a guardar en un Array.
         */
        ArrayList<Integer> puntuaciones = new ArrayList<>();
        System.out.println("Introduce las puntuaciones");
        int puntuacion;
        do {
            puntuacion = scanner.nextInt();
            if (puntuacion != -1) puntuaciones.add(puntuacion);
        } while (puntuacion != -1);

        /*
        Como el partido empieza con 0 puntos, vamos a ir restando la siguiente puntuación del array
        con la puntuación actual para ver si es canasta de 1, 2 o 3 puntos.
        Guardaremos en variables int los contadores de las canastas de 1, 2 y 3 puntos.
         */
        int canastasDe1 = 0;
        int canastasDe2 = 0;
        int canastasDe3 = 0;
        int puntuacionActual = 0; // El partido siempre empieza con 0 puntos
        for (int i = 0; i < puntuaciones.size(); i++) {
            // Extraemos la siguiente puntuación de nuestro array de puntuaciones
            int siguientePuntuacion = puntuaciones.get(i);

            // Restamos para ver si es canasta de 1, 2 o 3
            int canasta = siguientePuntuacion - puntuacionActual;
            switch (canasta) {
                case 1:
                    canastasDe1++;
                    break;
                case 2:
                    canastasDe2++;
                    break;
                case 3:
                    canastasDe3++;
                    break;
            }

            // Cuando marcan una canasta, la puntuación actual cambia.
            // Por ejemplo si llevaban 3 puntos y la siguiente canasta es de 2, la puntuación actual es de 3+2=5
            puntuacionActual = siguientePuntuacion;
        }

        // Ahora solo tenemos que imprimir por pantalla las canastas
        System.out.println("Canastas de 1 punto:  " + canastasDe1);
        System.out.println("Canastas de 2 puntos: " + canastasDe2);
        System.out.println("Canastas de 3 puntos: " + canastasDe3);

    }

}
