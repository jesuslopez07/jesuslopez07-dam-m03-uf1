package cat.itb.jesuslopez07.dam.m03.uf1.Coses;

import java.util.ArrayList;
import java.util.Scanner;

public class TenguiFalti {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        /*
        Como no sabemos cuantos cromos pueden introducir, vamos a utilizar el bucle do-while.
        Porque vamos a pedir cromos continuamente hasta que introduzcan "-1".
        Además, vamos a guardarnos todos los números de los cromos en un array para poder
        consultarlo siempre que el programa lo necesite.
         */
        System.out.println("Introduce los cromos repetidos: ");
        ArrayList<Integer> cromosRepetidos = new ArrayList<>();
        int cromo;
        do {
            cromo = scanner.nextInt();
            if (cromo != -1) cromosRepetidos.add(cromo);
        } while (cromo != -1);

        /*
        Hacemos exactamente lo mismo con los cromos que faltan.
         */
        System.out.println("Introduce los cromos que te faltan: ");
        ArrayList<Integer> cromosFaltan = new ArrayList<>();
        do {
            cromo = scanner.nextInt();
            if (cromo != -1) cromosFaltan.add(cromo);
        } while (cromo != -1);

        /*
        En este punto ya tenemos los 2 arrays con los cromos que están repetidos
        y los cromos que faltan.
        Ahora vamos a recorrernos el array de los cromos que faltan y vamos a buscar
        cada cromo dentro del array de cromos repetidos.
        De esta forma vamos a poder saber si se pueden cambiar o no.

        Como sabemos cuantas iteraciones necesitamos para recorrernos el array (array.size)
        vamos a elegir el bucle FOR.
         */
        for (int i = 0; i < cromosFaltan.size(); i++) {
            // Extraemos el cromo del array
            int cromoFalta = cromosFaltan.get(i);

            /*
            Ahora, dentro de este bucle, vamos a buscar el cromo que acabamos de sacar
            dentro del array de cromos repetidos.
             */
            for (int iRepetidos = 0; iRepetidos < cromosRepetidos.size(); iRepetidos++) {
                // Extraemos el cromo repetido
                int cromoRepetido = cromosRepetidos.get(iRepetidos);

                // Ahora ya podemos comparar los cromos y ver si se pueden cambiar
                if (cromoFalta == cromoRepetido) {
                    System.out.print(cromoFalta + " ");
                }
            }
        }

    }

}
