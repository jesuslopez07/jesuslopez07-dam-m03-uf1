package cat.itb.jesuslopez07.dam.m03.uf1.Coses;

import java.util.Scanner;

public class cosesArraysDinamics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Arrays Dinamics --->

        //Arrays vs List
        //Els arrays tenen una mida fixe i això els fa molt limitats.

        //List
        //Podem declarar llistes amb:
        //
        //// llista buida
        //List<Ingeger> list = new ArrayList<Integer>();
        //// llista amb valors
        //List<String> stringList = new ArrayList<String>(Arrays.asList("Mar", "Iu", "Ot"));
        //Accedir a una posició de l'array amb:
        //
        //int value = list.get(4);

        //Modificar a una posició de l'array amb:
        //
        //// set(int index, E element)
        //list.set(4, 6);
        //Consultat la llargada
        //
        //list.size();
        //For each
        //
        //for(int value: list){
        //  // do something
        //}

        //Print
        //
        //System.out.println(list);

        //List - operacions dinàmiques
        //Afegir un element
        //
        //// al final
        //list.add(15);
        //// o a una posició
        //// add(int index, E element)
        //list.add(2, 15);
        //Eliminar elements
        //
        //// l'element en una posició
        //list.remove(5);
        //// eliminar-los tots
        //list.clear();

        //
    }
}
