package cat.itb.jesuslopez07.dam.m03.uf1.Coses;

import java.util.ArrayList;
import java.util.Scanner;

public class HomeworkHelper {

    public static void main(String[] args) {

        System.out.println("Introduce las divisiones (introduce -1 para terminar)");

        Scanner scanner = new Scanner(System.in);

        // En este array vamos a guardar todas las divisiones que el usuario vaya introduciendo.
        ArrayList<String> divisiones = new ArrayList<>();

        // En esta variable guardamos la última linea que se ha introducido
        String linea;

        // Como vamos a ir pidiendo divisiones hasta que el usuario introduzca un -1
        // elegimos el bucle do-while. Porque siempre va a pedir como mínimo 1 linea
        // y parará cuando sea -1
        do {
            linea = scanner.nextLine();
            if (!linea.equals("-1")) divisiones.add(linea);
        } while (!linea.equals("-1"));

        // Ahora vamos a recorrernos todas las divisiones que el usuario ha ido introducioendo
        // y vamos a comprobar si es correcta o incorrecta.
        // Como sé exactamente el numero de iteraciones que tengo que hacer para recorrerme
        // el array con las divisiones <divisiones.size()>, el bucle es un for.
        for (int i = 0; i < divisiones.size(); i++) {
            // Extraemos la división del array
            String division = divisiones.get(i);

            // Guardamos en variables el dividendo, divisor, cuociente y residuo
            int dividendo = Character.getNumericValue(division.charAt(0));
            int divisor = Character.getNumericValue(division.charAt(2));
            int cuociente = Character.getNumericValue(division.charAt(4));
            int residuo = Character.getNumericValue(division.charAt(6));

            // Imprimimos por pantalla si la división es correcta o no:
            int cuocienteCorrecto = dividendo / divisor;
            int residuoCorrecto = dividendo % divisor;

            if (cuociente == cuocienteCorrecto && residuo == residuoCorrecto) {
                System.out.println("La división " + division + " es correcta.");
            } else {
                System.out.println("La división " + division + " es incorrecta.");
            }
        }

    }

}
