package cat.itb.jesuslopez07.dam.m03.uf1.Coses;

import java.util.Scanner;

public class DniLetterCalculator {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        /*
        Pedimos al usuario el DNI sin la letra
         */
        System.out.println("Introduce el DNI sin letra:");
        String dniSinLetra = scanner.nextLine();

        /*
        Como estamos guardando el DNI en una variable String (texto), lo convertimos a
        entero para poder aplicarle la fórmula al número.
         */
        int intDniSinLetra = Integer.valueOf(dniSinLetra);

        /*
        Aplicamos la fórmula y sacamos el DNI con la letra por pantalla.
         */
        int residuo = intDniSinLetra % 23;
        switch (residuo) {
            case 0:
                System.out.println(dniSinLetra + "T");
                break;
            case 1:
                System.out.println(dniSinLetra + "R");
                break;
            case 2:
                System.out.println(dniSinLetra + "W");
                break;
            case 3:
                System.out.println(dniSinLetra + "A");
                break;
            case 4:
                System.out.println(dniSinLetra + "G");
                break;
            case 5:
                System.out.println(dniSinLetra + "M");
                break;
            case 6:
                System.out.println(dniSinLetra + "Y");
                break;
            case 7:
                System.out.println(dniSinLetra + "F");
                break;
            case 8:
                System.out.println(dniSinLetra + "P");
                break;
            case 9:
                System.out.println(dniSinLetra + "D");
                break;
            case 10:
                System.out.println(dniSinLetra + "X");
                break;
            case 11:
                System.out.println(dniSinLetra + "B");
                break;
            case 12:
                System.out.println(dniSinLetra + "N");
                break;
            case 13:
                System.out.println(dniSinLetra + "J");
                break;
            case 14:
                System.out.println(dniSinLetra + "Z");
                break;
            case 15:
                System.out.println(dniSinLetra + "S");
                break;
            case 16:
                System.out.println(dniSinLetra + "Q");
                break;
            case 17:
                System.out.println(dniSinLetra + "V");
                break;
            case 18:
                System.out.println(dniSinLetra + "H");
                break;
            case 19:
                System.out.println(dniSinLetra + "L");
                break;
            case 20:
                System.out.println(dniSinLetra + "C");
                break;
            case 21:
                System.out.println(dniSinLetra + "K");
                break;
            case 22:
                System.out.println(dniSinLetra + "E");
                break;
        }
    }

}
