package cat.itb.jesuslopez07.dam.m03.uf1.Coses;

import java.util.Scanner;

public class cosasArrays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        //Arrays-->

        //El primer element està a la posició 0.
        //l'utlima la n-1.

        //Definició de una variable tipus array:
        //tipus[] nomVariable;
        //int[] values;

        //Amb valors
        //tipus[] nomVariable = {valor1, valor2, valor3, ...};
        //int[] values = {1,2,3};

        //Sense valor
        //O valor per defecte
        //tipus[] nomVariable = new tipus[size];
        //int[] values = new int[5];

        //Obtenció
        //int value = valors[5];
        //Assignació
        //values[5] = 12;
        //Llargada
        //int length = values.length

        //Valor per defecte
        //Tipus Simple
        //int -> 0
        //double -> 0
        //boolean -> false
        //Objectes
        //String -> null
        //null: absència de contingut

        //Imprimir array
        //Si fem un sout d'un array no en imprimirà el seu contingut.
        //Podem usar la funció toString definida dins de Arrays
        //System.out.println(Arrays.toString(array));

        //For each
        //for(int value : values){
        //  // ...
        //}

        //Matrius
        //int[][] matrix1 = {{1,2},{3,4}};
        //int[][] matrix2 = new int[10][2];
        //int value = matrix[0][0];
        //System.out.println(Arrays.deepToString(matrix1));
    }
}
