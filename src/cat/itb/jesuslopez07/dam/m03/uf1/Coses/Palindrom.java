package cat.itb.jesuslopez07.dam.m03.uf1.Coses;

import java.util.Scanner;

public class Palindrom {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        /*
        Pedimos al usuario que introduzca la frase
         */
        System.out.println("Introduzca la frase");
        String frase = scanner.nextLine();

        /*
        Como no se tiene que contar los espacios ni los carácteres de puntuación,
        vamos a utilizar la funcion <replace> para quitar esos carácteres.
        Por ejemplo en la palabra "HOLA" puedo utilizar la función <replace> para
        cambiar la "A" por una "I" y cambiar "HOLA" por "HOLI".
        Además, también podría cambiar una letra por nada. Por ejemplo, ahora voy
        A reemplazar la "H" por nada "". Así que ahora hemos cambiado "HOLA" por "OLA".
        Esto último es lo que vamos a hacer con los espacios y los carácteres especiales.
         */
        String fraseSinEspacios = frase;
        fraseSinEspacios = fraseSinEspacios.replace(" ", "");
        fraseSinEspacios = fraseSinEspacios.replace(".", "");
        fraseSinEspacios = fraseSinEspacios.replace(",", "");
        fraseSinEspacios = fraseSinEspacios.replace("'", "");
        fraseSinEspacios = fraseSinEspacios.replace("!", "");
        fraseSinEspacios = fraseSinEspacios.replace("?", "");
        fraseSinEspacios = fraseSinEspacios.replace("-", "");

        /*
        El enunciado nos dice que no se tienen que tener en cuenta las mayúsculas ni
        las minúsculas. Así que pasamos la frase a minúsculas para poder comparar bien.
        Por ejemplo, si introducimos "Luz azul", al revés es "luza zuL".
        Si comparamos el string, la "l" minúscula es diferente a la "L" mayúscula.
        Por eso lo pasamos a minúscula, porque queremos comparar la letra independientemente
        de si es mayúscula o minúscula.
         */
        fraseSinEspacios = fraseSinEspacios.toLowerCase();

        /*
        Ahora en la variable <fraseSinEspacios> tenemos la frase tal y como queremos.
        Por ejemplo la frase "Me llamo Jesus" en esta variable se estaría guardando
        así: "mellamojesus".
        Para comprobar si es un palíndromo, como sabemos que un string es una especie de
        array de carácteres, podemos recorrerlo al revés (de derecha a izquierda) y
        construir un nuevo string con la frase girada.
        Una vez hecho eso, ya podemos comparar los 2 string y determinar si es un palíndromo
        o no lo es.
         */
        String fraseSinEspaciosGirada = "";
        for (int i = fraseSinEspacios.length(); i > 0; i--) {
            fraseSinEspaciosGirada += fraseSinEspacios.charAt(i-1);
        }
        if (fraseSinEspacios.equals(fraseSinEspaciosGirada)) {
            System.out.println(frase + " es un palíndromo!");
        } else {
            System.out.println(frase + " no es un palíndromo.");
        }

    }

}
