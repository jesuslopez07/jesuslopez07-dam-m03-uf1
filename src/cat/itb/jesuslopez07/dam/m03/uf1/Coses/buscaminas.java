package cat.itb.jesuslopez07.dam.m03.uf1.Coses;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class buscaminas {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        //Creacion de la tabla del buscaminas, donde el usuario es el que pone las dimensiones...
        System.out.println("Introduce M:");
        int horitzontalMida = scanner.nextInt();
        System.out.println("Introduce N:");
        int verticalMida = scanner.nextInt();
        //Aqui el usuario tendra que poner el numero de bombas que quiere:
        System.out.println("Introduce nº minas:");
        int cantidadMinas = scanner.nextInt();

        int[][] taulaBuscaMines = new int[verticalMida][horitzontalMida]; //Aqui creamos el proceso para crear la tabla con las dimensiones que el usuario a definido.
        for (int i = 0; i < taulaBuscaMines.length; i++) {
            for (int j = 0; j < taulaBuscaMines.length; j++) {
                taulaBuscaMines[i][j] = 0;
            }
        }
        //El siguiente codigo for tiene la funcion de situar las n bombas en posiciones aleatorias de la tabla.
        for (int i = 0; i < cantidadMinas; i++) {
            int minaX = random.nextInt(horitzontalMida);
            int minaY = random.nextInt(verticalMida);
            taulaBuscaMines[minaX][minaY] = -1; //Nuestra bomba sera identificada como -1.
        }
        //El siguiente codigo "for" tiene la funcion de procesar los numeros segun la cercania de bombas...
        for (int i = 0; i < taulaBuscaMines.length; i++) {
            for (int j = 0; j < taulaBuscaMines.length; j++) {

                int iVecina = i;
                int jVecina = j;

                iVecina = i - 1;
                jVecina = j;
                if (taulaBuscaMines[i][j] != -1 && iVecina >= 0 && iVecina <= verticalMida - 1 && jVecina >= 0 && jVecina <= horitzontalMida - 1 && taulaBuscaMines[iVecina][jVecina] == -1) {
                    taulaBuscaMines[i][j]++;
                }

                iVecina = i - 1;
                jVecina = j + 1;
                if (taulaBuscaMines[i][j] != -1 && iVecina >= 0 && iVecina <= 9 && jVecina >= 0 && jVecina <= 9 && taulaBuscaMines[iVecina][jVecina] == -1) {
                    taulaBuscaMines[i][j]++;
                }
                iVecina = i;
                jVecina = j + 1;
                if (taulaBuscaMines[i][j] != -1 && iVecina >= 0 && iVecina <= 9 && jVecina >= 0 && jVecina <= 9 && taulaBuscaMines[iVecina][jVecina] == -1) {
                    taulaBuscaMines[i][j]++;
                }
                iVecina = i + 1;
                jVecina = j + 1;
                if (taulaBuscaMines[i][j] != -1 && iVecina >= 0 && iVecina <= 9 && jVecina >= 0 && jVecina <= 9 && taulaBuscaMines[iVecina][jVecina] == -1) {
                    taulaBuscaMines[i][j]++;
                }
                iVecina = i + 1;
                jVecina = j;
                if (taulaBuscaMines[i][j] != -1 && iVecina >= 0 && iVecina <= 9 && jVecina >= 0 && jVecina <= 9 && taulaBuscaMines[iVecina][jVecina] == -1) {
                    taulaBuscaMines[i][j]++;
                }
                iVecina = i + 1;
                jVecina = j - 1;
                if (taulaBuscaMines[i][j] != -1 && iVecina >= 0 && iVecina <= 9 && jVecina >= 0 && jVecina <= 9 && taulaBuscaMines[iVecina][jVecina] == -1) {
                    taulaBuscaMines[i][j]++;
                }
                iVecina = i;
                jVecina = j - 1;
                if (taulaBuscaMines[i][j] != -1 && iVecina >= 0 && iVecina <= 9 && jVecina >= 0 && jVecina <= 9 && taulaBuscaMines[iVecina][jVecina] == -1) {
                    taulaBuscaMines[i][j]++;
                }
                iVecina = i - 1;
                jVecina = j - 1;
                if (taulaBuscaMines[i][j] != -1 && iVecina >= 0 && iVecina <= 9 && jVecina >= 0 && jVecina <= 9 && taulaBuscaMines[iVecina][jVecina] == -1) {
                    taulaBuscaMines[i][j]++;
                }
            }
        }
        /*System.out.println("----------------------------------------------------------");
        System.out.println("CHULETA PARA SABER DONDE ESTAN LAS BOMBAS:");
        for (int i = 0; i < taulaBuscaMines.length; i++) {
            System.out.println(Arrays.toString(taulaBuscaMines[i]));
        }*/ //Unicamente se le quita el comentario para visualizar la chuleta. No afecta al juego directamente, acaso que quieras hacer trampas...

        System.out.println("----------------------------------------------------------"); //Utilizamos este STRING para ver una separacion grafica en las modificaciones.
        int[][] taulaBuscaMinesNET = new int[verticalMida][horitzontalMida]; //creamos la tabla del usuario. Donde indica lo primero que ve el usuario inicialmente.
        for (int i = 0; i < taulaBuscaMinesNET.length; i++) {
            for (int j = 0; j < taulaBuscaMinesNET.length; j++) {
                taulaBuscaMinesNET[i][j] = 0;
            }
            System.out.println(Arrays.toString(taulaBuscaMinesNET[i]));//Solo que se le mostrara al usuario, una tabla vacia con valores nulos (0).
        }

        //Aqui empieza el juego de verdad, el usuario tendra que introducir unas coordenadas, y la tabla se le modificara graficamente revelando informacion en aquella coordenada puesta.
        System.out.println("Introduce la coordenada X");
        int x = scanner.nextInt();
        System.out.println("Introduce la coordenada Y");
        int y = scanner.nextInt();

        int valorTaula = taulaBuscaMines[x][y];
        //Aqui pues se le revela la info en la coordenada puesta, mientras el valor de la coordenada no sea -1. Si es -1, el juego se acaba.
        System.out.println("----------------------------------------------------------");
        for (int i = 0; i < taulaBuscaMinesNET.length; i++) {
            for (int j = 0; j < taulaBuscaMinesNET.length; j++) {
                taulaBuscaMinesNET[x][y] = valorTaula;
            }
            System.out.println(Arrays.toString(taulaBuscaMinesNET[i]));
        }
        if (valorTaula == -1){
            System.out.println("Has perdut...");
        }else
            System.out.println("No ha passat res... Pots continuar.");

        while(valorTaula != -1){
            System.out.println("Introduce la coordenada X");
            x = scanner.nextInt();
            System.out.println("Introduce la coordenada Y");
            y = scanner.nextInt();

            valorTaula = taulaBuscaMines[x][y];

            System.out.println("----------------------------------------------------------");
            for (int i = 0; i < taulaBuscaMinesNET.length; i++) {
                for (int j = 0; j < taulaBuscaMinesNET.length; j++) {
                    taulaBuscaMinesNET[x][y] = valorTaula;
                }
                System.out.println(Arrays.toString(taulaBuscaMinesNET[i]));
            }
            if (valorTaula == -1){
                System.out.println("Has perdut...");
            }else
                System.out.println("No ha passat res... Pots continuar.");
        }

    }
}
