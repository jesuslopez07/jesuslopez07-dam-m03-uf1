package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int f1 = 1;
        int f2 = 0;
        int f = 0;

        if (n < 1)
            n = scanner.nextInt();
        for (int i = 0; i < n; i++){
            f = f1 + f2;
            f1 = f;
            if (i == 1 || i == 2)
                f2 = 1;
            else
                f2 = f - f1;
        }

        System.out.println(f);

    }
}
