package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class DotLine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();

        for(int i = 1; i <= n; i++) {
            System.out.print(".");
        }
        //Salt de linia final
        System.out.println();
    }
}
