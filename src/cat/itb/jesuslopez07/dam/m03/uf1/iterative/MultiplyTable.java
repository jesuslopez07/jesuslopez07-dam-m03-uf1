package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class MultiplyTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n = scanner.nextInt();
        int contador = 1;

        while (contador <= 9) {
            int resultat = contador * n;
            System.out.println(contador + " * " + n + " = " + resultat);
            contador++;
        }


    }
}
