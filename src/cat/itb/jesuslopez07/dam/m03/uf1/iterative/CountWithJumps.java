package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class CountWithJumps {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int nFinal = scanner.nextInt();
        int nSalts = scanner.nextInt();

        for (int i = 1; i <= nFinal; i += nSalts ){
            if(i != 1)
                System.out.print(" ");
            System.out.print(i);
        }
    }
}
