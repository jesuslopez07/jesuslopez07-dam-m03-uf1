package cat.itb.jesuslopez07.dam.m03.uf1.iterative.practice;

import java.util.Locale;
import java.util.Scanner;

public class DivideUntil0 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n = scanner.nextInt();
        int contadorDivison = 0;
        int contadorResta = 0;

        while (!(n == 0)){
            if (n % 2 == 0){
                n = n / 2;
                contadorDivison++;
            }
            if (n % 2 == 1){
                n = n - 1;
                contadorResta++;
            }
        }
        System.out.println("Divisions: "+contadorDivison);
        System.out.println("Restes: "+contadorResta);
    }
}
