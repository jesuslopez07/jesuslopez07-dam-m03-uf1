package cat.itb.jesuslopez07.dam.m03.uf1.iterative.practice;

import java.util.Locale;
import java.util.Scanner;

public class IsPowerOf2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int n = scanner.nextInt();

        while (!(n == 1)){
            if (n == 0){
                n = 0;
                break;
            }
            else if (n % 2 == 0){
                n = n / 2;
            }
            else{
                n = 0;
                break;
            }
        }
        if(n == 1)
            System.out.println("true");
        else
            System.out.println("false");

    }
}
