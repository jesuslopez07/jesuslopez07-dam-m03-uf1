package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class CountDown {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int num = scanner.nextInt();

        for (int i = num; i >= 1; i--)
            System.out.print(i);
    }
}
