package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Scanner;

public class GoTournamentGreatestScore {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String nom = scanner.nextLine();
        String guanyador = "";
        int puntGuanyadora = -1;

        while (!nom.equals("END")){
            int punt = scanner.nextInt();
            scanner.nextLine();

            if (punt > puntGuanyadora){
                guanyador = nom;
                puntGuanyadora = punt;
            }

            nom = scanner.nextLine();
        }

        System.out.println(guanyador + ": " + puntGuanyadora);

    }

}
