package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class OnlyVowels {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nChar= scanner.nextInt();

        for(int i=0; i<nChar; i++){
            String lletraStr = scanner.next();
            //char lletra = lletraStr.charAt(0);

            //comprove que es una vocal
            if(lletraStr.equals("a") || lletraStr.equals("e") || lletraStr.equals("i")
            || lletraStr.equals("o") || lletraStr.equals("u"))
                System.out.println(lletraStr);
        }
    }
}
