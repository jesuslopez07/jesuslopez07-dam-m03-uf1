package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class NumberBetweenOneAndFive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int num;

        do{
            System.out.println("Introdueix un nombre entre 1 i 5: ");
            num = scanner.nextInt();

        }while (!(num >= 1 && num <= 5));

        System.out.println("Has introduit: "+ num);

    }
}
