package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

public class MultiplyTableFull {
    public static void main(String[] args) {

        for (int i = 1; i < 10; i++){
            for (int j = 1; j < 10; j++) {
                int operation = i * j;
                if(j>1)
                    System.out.print("-");
                System.out.printf("%2d", operation);
            }
            System.out.println();
        }
    }
}
