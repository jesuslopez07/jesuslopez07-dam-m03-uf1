package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class LetsCountBetween {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();

        for (int i = num1 + 1; i < num2; i++)
            System.out.print(i);
    }
}
