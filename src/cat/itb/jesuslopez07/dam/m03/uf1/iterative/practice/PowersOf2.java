package cat.itb.jesuslopez07.dam.m03.uf1.iterative.practice;

import java.util.Scanner;

public class PowersOf2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = 1;

        for (int i = 0; i <= n; i++) {
            System.out.println("2^" + i + " = " + p);
            p = p*2;

        }

        System.out.println();

    }
}
