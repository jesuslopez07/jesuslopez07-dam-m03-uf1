package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class Piramid {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();

        for (int i = 1; i <= n; i++){
            for (int j = 1; j <= i; j++) {
                if(j>1)
                    System.out.print(" ");
                System.out.print("#");
            }
            System.out.println();
        }
    }
}
