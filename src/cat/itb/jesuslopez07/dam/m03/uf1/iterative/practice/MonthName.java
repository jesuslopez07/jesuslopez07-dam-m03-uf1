package cat.itb.jesuslopez07.dam.m03.uf1.iterative.practice;

import java.util.Locale;
import java.util.Scanner;

public class MonthName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int nMonth;

        do{
            nMonth = scanner.nextInt();
            if(!(nMonth <= 12 && nMonth >= 1))
                System.out.println("Error. El mes "+nMonth+" no és vàlid.");
        }while (!(nMonth <= 12 && nMonth >= 1));
        switch (nMonth){
            case 1:
                System.out.println("Gener");
                break;
            case 2:
                System.out.println("Febrer");
                break;
            case 3:
                System.out.println("Març");
                break;
            case 4:
                System.out.println("Abril");
                break;
            case 5:
                System.out.println("Maig");
                break;
            case 6:
                System.out.println("Juny");
                break;
            case 7:
                System.out.println("Juliol");
                break;
            case 8:
                System.out.println("Agost");
                break;
            case 9:
                System.out.println("Setembre");
                break;
            case 10:
                System.out.println("Octubre");
                break;
            case 11:
                System.out.println("Novembre");
                break;
            case 12:
                System.out.println("Desembre");
                break;
            default:
        }
    }
}
