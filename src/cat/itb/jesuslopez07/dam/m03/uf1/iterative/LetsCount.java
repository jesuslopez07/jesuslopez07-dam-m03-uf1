package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Locale;
import java.util.Scanner;

public class LetsCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int num = scanner.nextInt();

        int contador = 1;
        while (contador <= num) {
            System.out.print(contador);
            contador++;
        }
    }
}
