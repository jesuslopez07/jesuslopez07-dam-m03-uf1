package cat.itb.jesuslopez07.dam.m03.uf1.iterative;

import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int factorial = 1;
        for (int i = 1; i <= n; n--){
            factorial = n * factorial;
        }
        System.out.println(factorial);
    }
}
