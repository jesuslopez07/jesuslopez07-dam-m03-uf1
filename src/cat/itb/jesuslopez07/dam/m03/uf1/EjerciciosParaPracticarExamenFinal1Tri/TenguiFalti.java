package cat.itb.jesuslopez07.dam.m03.uf1.EjerciciosParaPracticarExamenFinal1Tri;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TenguiFalti {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> listaCromosRepetidos = new ArrayList<Integer>();
        int cromosRepetidos = scanner.nextInt();

        while (cromosRepetidos != -1) {
            listaCromosRepetidos.add(cromosRepetidos);
            cromosRepetidos = scanner.nextInt();
        }

        List<Integer> listaCromosFaltantes = new ArrayList<Integer>();
        int cromosFaltantes = scanner.nextInt();

        while (cromosFaltantes != -1) {
            listaCromosFaltantes.add(cromosFaltantes);
            cromosFaltantes = scanner.nextInt();
        }

        for (int i = 0; i < listaCromosFaltantes.size(); i++) {
            int cromoFaltante = listaCromosFaltantes.get(i);
            for (int j = 0; j < listaCromosRepetidos.size(); j++) {
                int cromoRepetido = listaCromosRepetidos.get(j);
                if (cromoRepetido == cromoFaltante) {
                    System.out.println(cromoFaltante);
                }
            }
        }
    }
}
