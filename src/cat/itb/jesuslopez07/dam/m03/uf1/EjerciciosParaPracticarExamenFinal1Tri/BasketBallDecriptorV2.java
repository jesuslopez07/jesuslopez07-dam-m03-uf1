package cat.itb.jesuslopez07.dam.m03.uf1.EjerciciosParaPracticarExamenFinal1Tri;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BasketBallDecriptorV2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> listaDePuntuacion = new ArrayList<Integer>();

        int n = 0;

        while (n != -1) {
            listaDePuntuacion.add(n);
            n = scanner.nextInt();
        }
        int [] contador = new int[4];
        for (int i = 1; i < listaDePuntuacion.size(); i++) {
            int operacion = listaDePuntuacion.get(i) - listaDePuntuacion.get(i-1);
            contador[operacion]++;
        }
        System.out.printf("cistelles d'un punt: %d\ncinstelles de dos punts: %d\ncistelles de tres punts: %d", contador[1], contador[2], contador[3]);

    }
}
