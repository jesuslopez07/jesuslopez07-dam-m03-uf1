package cat.itb.jesuslopez07.dam.m03.uf1.EjerciciosParaPracticarExamenFinal1Tri;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HomeworkHelperV3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            int dividend = scanner.nextInt();
            if (dividend == -1){
                break;
            }
            int divisor = scanner.nextInt();
            int quocient = scanner.nextInt();
            int residu = scanner.nextInt();

            int operacionQuocient = dividend/divisor;
            int operacionResidu = dividend%divisor;

            //Hay que destacar que aplicando un boolean utilizando la siguiente formula, quedaria asi:
            //boolean esCorrecte = divisor * quocient + residu == dividend; --Obviament, el if s'hauria de canviar pel
            //boolean...

            if(operacionQuocient == quocient && operacionResidu == residu){
                System.out.println("correcte");
            }else
                System.out.println("error");
            //sout(esCorrecte ? "correcte":"error"); --que seria lo mateix d'allò del if.
        }
    }
}

