package cat.itb.jesuslopez07.dam.m03.uf1.EjerciciosParaPracticarExamenFinal1Tri;

import java.util.Scanner;

public class DniLetterCalculatorV2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[] lletraDNI = {
                'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D',
                'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'
        };
        int numDNI = scanner.nextInt();
        int posicion = numDNI%23;
        char valor = lletraDNI[posicion];
        System.out.printf("%d%c", numDNI, valor);
    }
}
