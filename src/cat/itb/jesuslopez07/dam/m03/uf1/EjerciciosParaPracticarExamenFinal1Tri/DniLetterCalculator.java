package cat.itb.jesuslopez07.dam.m03.uf1.EjerciciosParaPracticarExamenFinal1Tri;

import java.util.Scanner;

public class DniLetterCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numerosDNI = scanner.nextInt();

        String numeroMaxEsOcho = Integer.toString(numerosDNI);

        if (numeroMaxEsOcho.length() == 8){
            int operacion = numerosDNI%23;
            System.out.println(operacion);
            switch (operacion){
                case 0:
                    System.out.println(numerosDNI+"T");
                    break;
                case 1:
                    System.out.println(numerosDNI+"R");
                    break;
                case 2:
                    System.out.println(numerosDNI+"W");
                    break;
                case 3:
                    System.out.println(numerosDNI+"A");
                    break;
                case 4:
                    System.out.println(numerosDNI+"G");
                    break;
                case 5:
                    System.out.println(numerosDNI+"M");
                    break;
                case 6:
                    System.out.println(numerosDNI+"Y");
                    break;
                case 7:
                    System.out.println(numerosDNI+"F");
                    break;
                case 8:
                    System.out.println(numerosDNI+"P");
                    break;
                case 9:
                    System.out.println(numerosDNI+"D");
                    break;
                case 10:
                    System.out.println(numerosDNI+"X");
                    break;
                case 11:
                    System.out.println(numerosDNI+"B");
                    break;
                case 12:
                    System.out.println(numerosDNI+"N");
                    break;
                case 13:
                    System.out.println(numerosDNI+"J");
                    break;
                case 14:
                    System.out.println(numerosDNI+"Z");
                    break;
                case 15:
                    System.out.println(numerosDNI+"S");
                    break;
                case 16:
                    System.out.println(numerosDNI+"Q");
                    break;
                case 17:
                    System.out.println(numerosDNI+"V");
                    break;
                case 18:
                    System.out.println(numerosDNI+"H");
                    break;
                case 19:
                    System.out.println(numerosDNI+"L");
                    break;
                case 20:
                    System.out.println(numerosDNI+"C");
                    break;
                case 21:
                    System.out.println(numerosDNI+"K");
                    break;
                case 22:
                    System.out.println(numerosDNI+"E");
                    break;
            }
        }else
            System.out.println("error");
    }
}
