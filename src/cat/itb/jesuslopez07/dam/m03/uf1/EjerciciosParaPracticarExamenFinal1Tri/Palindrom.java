package cat.itb.jesuslopez07.dam.m03.uf1.EjerciciosParaPracticarExamenFinal1Tri;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Character>fraseNormal = new ArrayList<Character>();

        String frase = scanner.nextLine();
        frase = frase.replace(" ", "");
        frase = frase.replace(".", "");
        frase = frase.replace(",", "");
        frase = frase.replace("'", "");
        frase = frase.replace("!", "");
        frase = frase.replace("?", "");
        frase = frase.replace("-", "");

        for (int i = 0; i < frase.length(); i++){
            fraseNormal.add(frase.charAt(i));
        }

        List<Character> fraseAlReves = new ArrayList<Character>();

        for (int i = fraseNormal.size()-1; i >= 0; i--){
            fraseAlReves.add(frase.charAt(i));
        }

        boolean esPalindrom = true;

        for (int i = 0; i < fraseNormal.size(); i++){
            char charNormal = fraseNormal.get(i);
            char charAlReves = fraseAlReves.get(i);

            if(charNormal != charAlReves){
                esPalindrom = false;
            }
        }
        if (esPalindrom==true){
            System.out.println("true");
        }else
            System.out.println("false");
    }
}
