package cat.itb.jesuslopez07.dam.m03.uf1.EjerciciosParaPracticarExamenFinal1Tri;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BasketBallDecriptor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> listaDePuntuacion = new ArrayList<Integer>();

        int n = 0;

        while (n != -1) {
            listaDePuntuacion.add(n);
            n = scanner.nextInt();
        }
        int contador1Punt = 0;
        int contador2Punt = 0;
        int contador3Punt = 0;
        for (int i = 1; i < listaDePuntuacion.size(); i++) {
            int operacion = listaDePuntuacion.get(i) - listaDePuntuacion.get(i-1);
            if (operacion == 1) {
                contador1Punt++;
            } else if (operacion == 2) {
                contador2Punt++;
            } else if (operacion == 3) {
                contador3Punt++;
            }
        }
        System.out.printf("cistelles d'un punt: %d\ncinstelles de dos punts: %d\ncistelles de tres punts: %d", contador1Punt, contador2Punt, contador3Punt);

    }
}
