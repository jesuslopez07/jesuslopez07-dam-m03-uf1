package cat.itb.jesuslopez07.dam.m03.uf1.EjerciciosParaPracticarExamenFinal1Tri;

import java.util.Scanner;

public class HomeworkHelper {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String linia;

        int posicion = 0;

        String[] linias = new String[100];

        do {
            linia = scanner.nextLine();
            linias[posicion] = linia;
            posicion++;
        }while (!(linia.equals("-1")));
        //System.out.println(Arrays.toString(linias));
        for (int i = 0; i < linias.length; i++){
            if (!(linias[i] == null || linias[i].equals("-1"))){
                //composicion en linias:
                char charDividend = linias[i].charAt(0);
                String stringDividend = String.valueOf(charDividend);
                int dividend = Integer.parseInt(stringDividend);
                //forma agrupada:
                int divisor = Integer.parseInt(String.valueOf(linias[i].charAt(2)));
                int quocient = Integer.parseInt(String.valueOf(linias[i].charAt(4)));
                int residu = Integer.parseInt(String.valueOf(linias[i].charAt(6)));

                int operacion = dividend/divisor;
                int operacionResiduo = dividend%divisor;

                if(operacion == quocient && operacionResiduo == residu){
                    System.out.println("correcte");
                }else
                    System.out.println("error");
            }
        }
    }
}

