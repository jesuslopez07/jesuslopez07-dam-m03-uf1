package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class NiceIsValidNote {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int billet = scanner.nextInt();

        boolean IsValidNote = billet ==5
                || billet == 10
                || billet == 20
                || billet == 50
                || billet == 100
                || billet == 200
                || billet == 500;
        if (IsValidNote){
            System.out.println("billet vàlid");
        }else
            System.out.println("billet invàlid");
    }
}
