package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class ExamGrade {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        System.out.println("Nota: ");

            int nota = scanner.nextInt();

            if (nota > 10)
                System.out.println("Nota invàlida");
            else if (nota >= 9)
                System.out.println("Exelent");
            else if (nota >= 7)
                System.out.println("Notable");
            else if (nota == 6)
                System.out.println("Bé");
            else if (nota == 5)
                System.out.println("Suficient");
            else if (nota >= 0)
                System.out.println("Suspès");
            else
                System.out.println("Nota invàlida");
    }
}
