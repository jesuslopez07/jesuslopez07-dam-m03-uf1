package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class NextSecond2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int hores = scanner.nextInt();
        int minuts = scanner.nextInt();
        int segons = scanner.nextInt();

        System.out.printf("Són les %02d:%02d:%02d\n", hores, minuts, segons);

        segons += 1;
        if(segons >= 60){
            minuts += segons / 60;
            segons = segons % 60;
        }

        if(minuts >= 60){
            hores += 1;
            minuts = 0;
        }

        if(hores >= 24){
            hores = 0;
        }

        System.out.printf("Són les %02d:%02d:%02d\n", hores, minuts, segons);
    }
}
