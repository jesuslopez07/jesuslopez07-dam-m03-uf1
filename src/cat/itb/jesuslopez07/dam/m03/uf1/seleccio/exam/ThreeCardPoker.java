package cat.itb.jesuslopez07.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class ThreeCardPoker {
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();
        int num3 = scanner.nextInt();

        boolean trio = num1 == num2 && num2 == num3;
        boolean escala = num1 < num2 && num2 < num3;
        boolean parella = (num1 == num2 && num2 != num3) ||
                (num1 != num2 && num2 == num3) ||
                (num1 == num3 && num3 != num2);
        boolean numeroAlt = num1 != num2 && num2 != num3;

        if (trio)
            System.out.println("Trio");
        else if (escala)
            System.out.println("Escala");
        else if (parella)
            System.out.println("Parella");
        else if (numeroAlt)
            System.out.println("Número alt");
        else
            System.out.println("No identificat");
    }
}
