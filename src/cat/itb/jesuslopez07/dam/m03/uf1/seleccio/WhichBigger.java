package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class WhichBigger {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int num1 = scanner.nextInt();
        int num2 = scanner.nextInt();

        int resultat;

        if (num1 > num2)
            resultat = num1;
        else
            resultat = num2;
        System.out.println(resultat);
    }
}
