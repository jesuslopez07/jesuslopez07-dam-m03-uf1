package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class NiceIsLegalAge {
    public static void main(String[] args) {
        System.out.println("Quina edat tens?");

        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int edat = scanner.nextInt();

        if (edat >= 18)
        System.out.println("ets major d'edat");

        else
        System.out.println("no ets major d'edat");
    }
}
