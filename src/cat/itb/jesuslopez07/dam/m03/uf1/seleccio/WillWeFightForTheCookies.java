package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class WillWeFightForTheCookies {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        System.out.println("Quantes persones sou?");

        int numPersones = scanner.nextInt();

        System.out.println("Quantes galetes?");

        int numGaletes = scanner.nextInt();

        int numGaletesPersona = numGaletes / numPersones;
        int galetesSobrants = numGaletes % numPersones;

        System.out.println(numGaletesPersona);

        if (galetesSobrants == 0)
            System.out.println("Let's eat!");

        else
            System.out.println("Let's fight!");
    }
}
