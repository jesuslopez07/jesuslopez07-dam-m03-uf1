package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class IsLeapYear {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int year = scanner.nextInt();

        boolean yearIsLeap = (year % 4 == 0) &&
                (year % 100 != 0) ||
                (year % 400 == 0);

        if (yearIsLeap)
            System.out.println(year+" és any de traspàs.");
        else
            System.out.println(year+" no és any de traspàs.");
    }
}
