package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class IdentikitGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String hair = scanner.next();
        String eyes = scanner.next();
        String nose = scanner.next();
        String mouth = scanner.next();

        switch (hair) {
            case "arrissats":
                System.out.println("@@@@@");
                break;
            case "llisos":
                System.out.println("VVVVV");
                break;
            case "pentinats":
                System.out.println("XXXXX");
                break;
            default:
                System.out.println("Cabells invàlids.");
        }
        switch (eyes){
            case "aclucats":
                System.out.println(".-.-.");
                break;
            case "rodons":
                System.out.println(".o-o.");
                break;
            case "estrellats":
                System.out.println(".*-*.");
                break;
            default:
                System.out.println("Ulls invàlids");
        }
        switch (nose){
            case "axafat":
                System.out.println("..O..");
                break;
            case "arromangat":
                System.out.println("..C..");
                break;
            case "aguilenc":
                System.out.println("..V..");
                break;
            default:
                System.out.println("Nas invàlids");
        }
        switch (mouth){
            case "normal":
                System.out.println(".===.");
                break;
            case "bigoti":
                System.out.println(".///.");
                break;
            case "dents-sortides":
                System.out.println(".www.");
                break;
            default:
                System.out.println("Boca invàlida");
        }
    }
}

