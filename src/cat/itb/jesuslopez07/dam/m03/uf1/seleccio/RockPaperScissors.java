package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int p = scanner.nextInt();
        int s = scanner.nextInt();

        boolean guanyaPrimer = (p == 1 && s == 3) ||
                (p == 2 && s == 1) ||
                (p == 3 && s == 2);

        if ( p == s)
            System.out.println("Empat!");
        else if ( guanyaPrimer )
            System.out.println("Guanya Primer!");
        else
            System.out.println("Guanya Segon!");
    }
}
