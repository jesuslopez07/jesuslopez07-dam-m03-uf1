package cat.itb.jesuslopez07.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class LaMasovera {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String diaSetmana = scanner.next();

        switch (diaSetmana) {
            case "dilluns":
                System.out.println("Compra llums");
                break;
            case "dimarts":
                System.out.println("Compra naps");
                break;
            case "dimecres":
                System.out.println("Compra nespres");
                break;
            case "dijous":
                System.out.println("Compra nous");
                break;
            case "divendres":
                System.out.println("Faves tendres");
                break;
            case "dissabte":
                System.out.println("Tot s'ho gasta");
                break;
            case "diumenge":
                System.out.println("Tot s'ho menja");
                break;
        }
    }
}
