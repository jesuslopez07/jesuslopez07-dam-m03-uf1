package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class AbsoluteNumber2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        System.out.println("Insereix nombre, si es negatiu, es torna en valor absolut: ");

        int value = scanner.nextInt();

        int absoluteValue = 0;

        if (value < absoluteValue)
            absoluteValue = -value;
        else
            absoluteValue = value;
        System.out.println(absoluteValue);

    }
}
