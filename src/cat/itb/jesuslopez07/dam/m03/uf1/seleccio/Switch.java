package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class Switch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String diaSetmana = scanner.next();

        switch (diaSetmana) {
            case "Dilluns":
            case "Dimarts":
            case "Dimecres":
            case "Dijous":
            case "Divendres":
                System.out.println("Entre setmana.");
                break;
            case "Dissabte":
            case "Diumenge":
                System.out.println("Cap de setmana.");
                break;
            default:
                System.out.println("Dia invàlid.");
        }
    }
}

