package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class LetsFightForTheCookies {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int nPersones = scanner.nextInt();
        int nGaletes = scanner.nextInt();

        int nombreGaletesPersona = nGaletes / nPersones;
        int galetesSobrants = nGaletes % nPersones;

        System.out.printf("Galetes per persona: %d\n", nombreGaletesPersona);
        System.out.printf("Galetes sobrants: %d\n", galetesSobrants);

        if(galetesSobrants == 0)
            System.out.println("Let's Eat!");
        else
            System.out.println("Let's Fight!");
    }
}

