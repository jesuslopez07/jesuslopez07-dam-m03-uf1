package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class IsLeap2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int any = scanner.nextInt();

        // - és divisible entre 4
        // - no és divisible entre 100
        // - és divisible entre 400

        boolean isLeap = (any % 4 == 0) &&
                ((any % 100 != 0) || (any % 400 == 0));

        if(isLeap)
            System.out.printf("%d és any de traspàs\n", any);
        else
            System.out.printf("%d no és any de traspàs\n", any);

    }
}

