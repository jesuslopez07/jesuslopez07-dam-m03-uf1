package cat.itb.jesuslopez07.dam.m03.uf1.seleccio.exam;

import java.util.Locale;
import java.util.Scanner;

public class CowType {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int edat = scanner.nextInt();
        // 1 = mascle 2 = femella
        int genere = scanner.nextInt();
        // 1 = capat 2= no capat
        int capatono = scanner.nextInt();

        boolean vedell = edat < 2;
        boolean vaca = edat >= 2 && genere == 2;
        boolean toro = edat >= 2 && genere == 1 && capatono == 1;
        boolean bou = edat >= 2 && genere == 1 && capatono == 2;

        if (vedell)
            System.out.println("vedell");
        else if (vaca)
            System.out.println("vaca");
        else if (toro)
            System.out.println("toro");
        else if (bou)
            System.out.println("bou");
        else
            System.out.println("animal no identificat");
    }
}
