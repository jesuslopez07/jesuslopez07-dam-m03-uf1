package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class NextSecond {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int hores = scanner.nextInt();
        int minuts = scanner.nextInt();
        int segons = scanner.nextInt();


        if (segons >= 60)
            minuts = segons / 60;
            segons = segons % 60;
        System.out.printf("Son les %02d:%02d:%02d", hores, minuts, segons);}
}
