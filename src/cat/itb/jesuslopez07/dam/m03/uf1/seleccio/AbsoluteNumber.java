package cat.itb.jesuslopez07.dam.m03.uf1.seleccio;

import java.util.Locale;
import java.util.Scanner;

public class AbsoluteNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        System.out.println("Insereix nombre, si es negatiu, es torna en valor absolut:");

        int num1 = scanner.nextInt();

        if (num1 < 0)
            num1 = Math.abs(scanner.nextInt());
        System.out.println(num1);

    }
}
