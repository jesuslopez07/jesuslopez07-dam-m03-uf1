package cat.itb.jesuslopez07.dam.m03.uf1.generalexam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GenderGuesser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> listaDeParaules = new ArrayList<>();
        String paraula = scanner.nextLine();
        while (!(paraula.equals("END"))){
            listaDeParaules.add(paraula);
            paraula = scanner.nextLine();
        }
        System.out.println(listaDeParaules);
        for (int i = 0; i < listaDeParaules.size(); i++){
            String paraula1 = listaDeParaules.get(i);
            for (int j = paraula.length() -1; j == paraula.length(); j++){
                if (paraula1.charAt(j) == 's'){
                    paraula1 = paraula.replace(paraula, "plural");
                }else if (paraula.charAt(j) == 'a'){
                    paraula1 = paraula.replace(paraula, "femení");
                }else if(paraula.equals("END"))
                    paraula1 = paraula.replace(paraula, "");
                else
                    paraula1 = paraula.replace(paraula, "masculí");
            }
        }
        System.out.println(listaDeParaules);
    }
}
