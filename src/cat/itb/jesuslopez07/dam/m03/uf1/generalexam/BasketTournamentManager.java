package cat.itb.jesuslopez07.dam.m03.uf1.generalexam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class BasketTournamentManager {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> listaDeEquipos = new ArrayList<>();

        int nEquip = scanner.nextInt();

        while (nEquip != -1){
            if (nEquip < 1 && nEquip > 8){
                break;
            }
            listaDeEquipos.add(nEquip);
            nEquip = scanner.nextInt();
        }
        System.out.println(listaDeEquipos);
        int[] contador = new int[8];
        for (int i = 0; i < listaDeEquipos.size(); i++){
            int victories = listaDeEquipos.get(i);
            if(victories != -1){
            for (int j = 0; i < listaDeEquipos.size(); j++){
                int vegadesGuanyat = listaDeEquipos.get(j);
                if (victories == vegadesGuanyat){
                    contador[victories]++;
                    System.out.println("L'equip "+victories+" té "+ Arrays.toString(contador)+ " victories");
                }
            }
            }
        }
    }
}
