package cat.itb.jesuslopez07.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class BoxesOpenedCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int[] stateBoxes = new int[10];
        System.out.println(Arrays.toString(stateBoxes));

        int nombres = scanner.nextInt();
        while(nombres != -1)
        {
            // cavia estat
            stateBoxes[nombres]++;
            System.out.println(Arrays.toString(stateBoxes));

            nombres = scanner.nextInt();
        }
        System.out.println(Arrays.toString(stateBoxes));
    }
}
