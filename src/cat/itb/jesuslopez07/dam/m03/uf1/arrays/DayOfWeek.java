package cat.itb.jesuslopez07.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;

public class DayOfWeek {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int dia = scanner.nextInt();
        String[] diesSetmana = {"dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge"};
        System.out.println(diesSetmana[dia]);
    }
}
