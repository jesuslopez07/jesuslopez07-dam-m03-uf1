package cat.itb.jesuslopez07.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayConfigurator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int posicio = scanner.nextInt();
        int n = scanner.nextInt();

        int[] valors = new int[10];

        while (n != -1 || posicio != -1){
            valors[posicio] = n;
            System.out.println(Arrays.toString(valors));
            posicio = scanner.nextInt();
            n = scanner.nextInt();
        }
        System.out.println(Arrays.toString(valors));


    }
}
