package cat.itb.jesuslopez07.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class PushButtomPadlockSimulator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        boolean[] statePadlock = new boolean[8];
        System.out.println(Arrays.toString(statePadlock));

        int numActual = scanner.nextInt();
        while(numActual != -1)
        {
            // cavia estat
            statePadlock[numActual] = !statePadlock[numActual];
            System.out.println(Arrays.toString(statePadlock));

            numActual = scanner.nextInt();
        }
        System.out.println(Arrays.toString(statePadlock));
    }
}
