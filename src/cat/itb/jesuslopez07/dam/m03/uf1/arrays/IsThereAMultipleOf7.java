package cat.itb.jesuslopez07.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class IsThereAMultipleOf7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int[] values = {4,8,9,40,54,84,40,6,84,1,1,68,84,68,4,840,684,25,40,98,54,687,31,4894,
                468,46,84687,894,40,846,1681,618,161,846,84687,6,848};

        System.out.println(Arrays.toString(values));


    }
}
