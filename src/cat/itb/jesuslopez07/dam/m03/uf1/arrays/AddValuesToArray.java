package cat.itb.jesuslopez07.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class AddValuesToArray {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        double[] valors = new double[50];
        valors[0] = 31;
        valors[1] = 56;
        valors[19] = 12;
        valors[49] = 79;
        System.out.println(Arrays.toString(valors));


    }
}
