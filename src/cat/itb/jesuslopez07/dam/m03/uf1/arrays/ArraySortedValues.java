package cat.itb.jesuslopez07.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class ArraySortedValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in).useLocale(Locale.US);
        int n = scanner.nextInt();

        int[] valors = new int[n];

        for (int i = 0; i < valors.length; i++){
            valors[i] = scanner.nextInt();
        }
        boolean ordenats = true;
        for (int i = 1; i < valors.length; i++){
            if (valors[i-1] > valors[i]){
                ordenats = false;
            }
        }
        if (ordenats)
            System.out.println("ordenats");
        else
            System.out.println("desordenats");
    }
}
