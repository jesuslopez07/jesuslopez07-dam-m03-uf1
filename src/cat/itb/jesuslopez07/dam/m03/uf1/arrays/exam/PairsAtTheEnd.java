package cat.itb.jesuslopez07.dam.m03.uf1.arrays.exam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PairsAtTheEnd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> valors = new ArrayList<Integer>();
        int numActual = scanner.nextInt();

        while (numActual != -1){
            if (numActual % 2 == 0){
                valors.add(numActual);
            }else
                valors.add(0, numActual);
            numActual = scanner.nextInt();
        }
        System.out.println(valors);
    }
}
