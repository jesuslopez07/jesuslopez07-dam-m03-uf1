package cat.itb.jesuslopez07.dam.m03.uf1.arrays.exam;

import java.util.Scanner;

public class SumPositiveValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int[] valors = new int[n];

        int sumaTotal;

        for (int i = 0; i < valors.length; i++){
            valors[i] = scanner.nextInt();
            if (valors[i]>0){
                sumaTotal = valors[i-1]+valors[i];
                System.out.println(sumaTotal);
            }
        }

    }
}
