package cat.itb.jesuslopez07.dam.m03.uf1.arrays;

import java.util.Locale;
import java.util.Scanner;
import java.util.Arrays;

public class MatrixBoxesOpenedCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] matriu = new int[4][4];
        int x = 0;
        int y = 0;
        while (x != -1 && y != -1){
            x = scanner.nextInt();
            if (x == -1)
                break;
            y = scanner.nextInt();
            if (y == -1)
                break;
            matriu[x][y]++;
        }
        System.out.println(Arrays.deepToString(matriu));
    }

}
