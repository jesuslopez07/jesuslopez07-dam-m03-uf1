package cat.itb.jesuslopez07.dam.m03.uf1.strings.practice;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CristmasGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<String> generadorCarta = new ArrayList<>();

        String dataPasat = scanner.next();
        String dataActual = scanner.next();
        String nomPasat = scanner.next();
        String nomActual = scanner.next();
        String autorPasat = scanner.next();
        String autorActual = scanner.next();

        String carta = scanner.nextLine();
        carta = carta.replace(dataPasat, dataActual);
        carta = carta.replace(nomPasat, nomActual);
        carta = carta.replace(autorPasat, autorActual);

        while (!carta.equals("END")){
            generadorCarta.add(carta);
            carta = scanner.nextLine();
            carta = carta.replace(dataPasat, dataActual);
            carta = carta.replace(nomPasat, nomActual);
            carta = carta.replace(autorPasat, autorActual);
        }

        for (String i: generadorCarta) {
            System.out.print(i);
            System.out.println();
        }
    }
}
