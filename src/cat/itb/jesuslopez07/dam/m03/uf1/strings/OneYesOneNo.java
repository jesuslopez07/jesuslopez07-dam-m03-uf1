package cat.itb.jesuslopez07.dam.m03.uf1.strings;

import java.util.Scanner;

public class OneYesOneNo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String paraula = scanner.nextLine();

        char lletra;

        for (int i = 0; i < paraula.length(); i = i + 2){
            lletra = paraula.charAt(i);
            System.out.print(lletra);
        }
    }
}
