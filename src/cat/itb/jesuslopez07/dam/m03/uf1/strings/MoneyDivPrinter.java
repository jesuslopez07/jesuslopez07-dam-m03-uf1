package cat.itb.jesuslopez07.dam.m03.uf1.strings;

import java.util.Scanner;

public class MoneyDivPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double quantMoney = scanner.nextInt();
        double quantPers = scanner.nextInt();

        double moneyDiv = quantMoney/quantPers;


        System.out.printf("Si tenim %.2f€ i %.0f persones, toquen a %.2f€ per persona", quantMoney, quantPers, moneyDiv);
    }
}
