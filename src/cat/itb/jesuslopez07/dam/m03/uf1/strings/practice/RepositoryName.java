package cat.itb.jesuslopez07.dam.m03.uf1.strings.practice;

import java.util.Scanner;

public class RepositoryName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String nomRepositori = "";
        int modul = scanner.nextInt();
        int uf = scanner.nextInt();
        String correu = scanner.nextLine();
        correu = correu.replace(".", "");

        for(int i = 0; i < correu.length(); i++){
            String lletra = correu.substring(i+1,i+2);
            if (!lletra.equals("@")){
                nomRepositori += lletra;
            }else
                break;
        }
        if (modul < 10){
            System.out.printf("%s-dam-m0%d-uf%d", nomRepositori, modul, uf);
        }else
            System.out.printf("%s-dam-m%d-uf%d", nomRepositori, modul, uf);
    }
}
