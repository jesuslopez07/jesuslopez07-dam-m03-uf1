package cat.itb.jesuslopez07.dam.m03.uf1.strings.practice;

import java.util.Scanner;

public class SuperSecretText {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[] abecedario = {"a","b","c","d","e","f","g","h","i"
                ,"j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};

        int encriptar = scanner.nextInt();
        int numeroDelCifrado = scanner.nextInt(); //posiciones que avanza respecto a la letra inicial...
        String textoEncriptar = "";
        scanner.nextLine();
        if (encriptar == 1){
            String texto = scanner.nextLine();
            for (int i = 0; i < texto.length(); i++) {
                for (int j = 0; j < abecedario.length; j++) {
                    String lletra = texto.substring(i, i + 1);
                    if (lletra.equals(abecedario[j])){
                        lletra = abecedario[j + numeroDelCifrado];
                        textoEncriptar += lletra;
                    }
                    if (lletra.equals(" ")){
                        textoEncriptar += "'";
                        break;
                    }
                }
            }
            System.out.println(textoEncriptar);
        }else if (encriptar == 0){
            String texto = scanner.nextLine();
            for (int i = 0; i < texto.length(); i++) {
                for (int j = 0; j < abecedario.length; j++) {
                    String lletra = texto.substring(i, i + 1);
                    if (lletra.equals(abecedario[j])){
                        lletra = abecedario[j - numeroDelCifrado];
                        textoEncriptar += lletra;
                    }
                    if (lletra.equals("'")){
                        textoEncriptar += " ";
                        break;
                    }
                }
            }
            System.out.println(textoEncriptar);
        }

    }
}
