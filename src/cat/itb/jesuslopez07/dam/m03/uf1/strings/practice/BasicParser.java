package cat.itb.jesuslopez07.dam.m03.uf1.strings.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BasicParser {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> listaTextoConFormato = new ArrayList<String>();
        String texto = scanner.nextLine();
        while (!texto.equals("")){ //tambien se le puede aplicar el comando "trexto.isEmpty()" para indicar que no hay nada...
            listaTextoConFormato.add(texto);
            texto = scanner.nextLine().replace("**", "\u001B[1m");
            texto = texto.replace("--", "\u001B[3m");
            texto = texto.replace("++", "\u001B[0m");
            texto = texto.replace("//r//", "\u001B[31m");
            texto = texto.replace("//g//", "\u001B[32m");
            texto = texto.replace("//b//", "\u001B[34m");
            texto = texto.replace("//s//", "\u001B[39m");
        }
        for (String i : listaTextoConFormato) {
            System.out.print(i);
            System.out.println();
        }

    }
}
