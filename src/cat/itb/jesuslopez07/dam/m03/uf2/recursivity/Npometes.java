package cat.itb.jesuslopez07.dam.m03.uf2.recursivity;

import java.lang.String;
import java.util.Scanner;

public class Npometes {
    public static String getAppleSongStanza(int applesCount){
        return String.format("%1$d pometes té el pomer,%nde %1$d una, de %1$d una,%n" +
                "%1$d pometes té el pomer,%nde %1$d una en caigué.%nSi mireu el vent d'on vé%n" +
                "veureu el pomer com dansa,%nsi mireu el vent d'on vé%n" +
                "veureu com dansa el pomer.%n", applesCount);
    }

    public static void printSong(int inicial){
        if (inicial == 0){
            System.out.println(getAppleSongStanza(inicial));
        }else{
            System.out.println(getAppleSongStanza(inicial));
            printSong(inicial-1);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inicial = scanner.nextInt();

        printSong(inicial);
    }
}
