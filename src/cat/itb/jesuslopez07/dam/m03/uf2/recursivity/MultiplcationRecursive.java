package cat.itb.jesuslopez07.dam.m03.uf2.recursivity;

import java.sql.SQLOutput;
import java.util.Scanner;

public class MultiplcationRecursive {
    public static int multiplicationRecursive(int a, int b){
        if (b==1){
            return a;
        }else
            return a + multiplicationRecursive(a, b-1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        System.out.println(multiplicationRecursive(a, b));
    }
}
