package cat.itb.jesuslopez07.dam.m03.uf2.recursivity;

import java.util.Scanner;

public class DotLineRecursive {
    public static String dots(int nDots){
        if (nDots==1){
            return "."; //aixo es el mateix que definir l'objecte anteriorment, ex: String punt = "."; y després directament el cridem al return...
        }else
            return "." + dots(nDots-1);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int punts = scanner.nextInt();
        System.out.println(dots(punts));
    }
}
