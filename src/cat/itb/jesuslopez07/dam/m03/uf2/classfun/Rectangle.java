package cat.itb.jesuslopez07.dam.m03.uf2.classfun;

import java.util.Scanner;

public class Rectangle {
    private double altura;
    private double anchura;

    public Rectangle(double altura, double anchura){
        this.altura = altura;
        this.anchura = anchura;
    }

    public static Rectangle readRectangle(Scanner scanner){
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();
        return new Rectangle(width, height);
    }

    public double getAltura(){
        return this.altura;
    }

    public double getAnchura(){
        return this.anchura;
    }

    public void setAltura(){
        this.altura = altura;
    }

    public void setAnchura(){
        this.anchura = anchura;
    }

    //metodos para obtener los resultados tanto del area como el del perimetrode los rectangulos.
    public double getArea(){
        return altura*anchura;
    }

    public double getPerimetro(){
        return (2*(altura+anchura));
    }

    public String getRectangleInfo(){
        return String.format("Un rectangle de %.2f x %.2f té %.2f d'area i %.2f de perímetre.", this.getAltura(), this.getAnchura(), this.getArea(), this.getPerimetro());
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "altura=" + altura +
                ", anchura=" + anchura +
                '}';
    }
}
