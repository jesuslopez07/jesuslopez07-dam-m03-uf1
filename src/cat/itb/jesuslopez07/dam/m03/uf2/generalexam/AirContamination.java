package cat.itb.jesuslopez07.dam.m03.uf2.generalexam;

import java.util.Scanner;

public class AirContamination {

    private int contamination;

    public AirContamination(int contamination) {
        this.contamination = contamination;
    }

    public int getContamination() {
        return contamination;
    }

    public static AirContamination readContamination(Scanner scanner){
        int contamination = scanner.nextInt();
        return new AirContamination(contamination);
    }

    public static double average (AirContamination[] contamination){
        int sumatori = 0;
        for (int i = 0; i < contamination.length; i++) {
            int suma = sumatori + contamination[i].getContamination();
            sumatori = suma;
        }
        double mitjana = (double) sumatori/contamination.length;
        return mitjana;
    }

    public static int superiorAverage(AirContamination[] contamination){
        int contador = 0;
        double average = AirContamination.average(contamination);
        for (int i = 0; i < contamination.length; i++) {
            if (contamination[i].getContamination() > average){
                contador++;
            }
        }
        return contador;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        AirContamination[] contamination = new AirContamination[num];
        for (int i = 0; i < contamination.length; i++) {
            AirContamination d = AirContamination.readContamination(scanner);
            contamination[i] = d;
        }
        double average = AirContamination.average(contamination);
        int numSuperat = AirContamination.superiorAverage(contamination);
        System.out.printf("Contaminació mitjana: %.1f\n", average);
        System.out.printf("Dies amb amb contaminació superior: %d\n", numSuperat);
    }
}
