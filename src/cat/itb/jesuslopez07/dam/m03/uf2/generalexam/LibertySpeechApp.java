package cat.itb.jesuslopez07.dam.m03.uf2.generalexam;

import java.util.Scanner;

public class LibertySpeechApp {

    private String nom;
    private int casos;
    private double gravetat;

    public LibertySpeechApp(String nom, int casos, double gravetat){
        this.nom = nom;
        this.casos = casos;
        this.gravetat = gravetat;
    }

    //getters:

    public String getNom() {
        return nom;
    }

    public int getCasos() {
        return casos;
    }

    public double getGravetat() {
        return gravetat;
    }

    public double getScore(){
        return casos*gravetat;
    }

    //Metode per llegir:
    public static LibertySpeechApp readData(Scanner scanner){
        String nom = scanner.next();
        int casos = scanner.nextInt();
        double gravetat = scanner.nextDouble();
        return new LibertySpeechApp(nom, casos, gravetat);
    }
    //Metode per calcular casos totals i pais amb més casos:

    public static int maxCases(LibertySpeechApp[] data){
        int casos = 0;
        int maxCasos = 0;
        int lloc = 0;
        for (int i = 0; i < data.length; i++) {
            casos = data[i].getCasos();
            if(casos > maxCasos){
                maxCasos = casos;
                lloc = i;
            }
        }
        return lloc;
    }

    public static int totalCases(LibertySpeechApp[] data){
        int sumaTotal = 0;
        for (int i = 0; i < data.length; i++) {
            int suma = sumaTotal + data[i].getCasos();
            sumaTotal = suma;
        }
        return sumaTotal;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num = scanner.nextInt();
        LibertySpeechApp[] data = new LibertySpeechApp[num];
        for (int i = 0; i < data.length; i++) {
            LibertySpeechApp d = LibertySpeechApp.readData(scanner);
            data[i] = d;
        }
        System.out.println("---------- Paisos ----------");
        for (int i = 0; i < data.length; i++) {
            System.out.printf("%s - casos: %d - gravetat: %1.01f - puntuació: %1.01f\n", data[i].getNom(), data[i].getCasos(), data[i].getGravetat(), data[i].getScore());
        }
        System.out.println("---------- Resum ----------");
        int casosTotal = LibertySpeechApp.totalCases(data);
        System.out.printf("Casos totals: %d\n", casosTotal);
        int maxCasos = LibertySpeechApp.maxCases(data);
        System.out.printf("País amb més casos: %s", data[maxCasos].getNom());
    }
}
