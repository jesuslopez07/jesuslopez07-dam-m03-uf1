package cat.itb.jesuslopez07.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class OneProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        String nombre = scanner.nextLine();
        double precio = scanner.nextDouble();

        Product producto = new Product(nombre, precio);
        System.out.printf("El producte %s val %.2f€", nombre, precio);
    }
}
