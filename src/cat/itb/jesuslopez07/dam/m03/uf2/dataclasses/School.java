package cat.itb.jesuslopez07.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class School {
    private String name;
    private String address;
    private String city;
    private String postalCode;

    public static School readSchool(Scanner scanner) {
        String name = scanner.nextLine();
        String address = scanner.nextLine();
        String city = scanner.nextLine();
        String postalCode = scanner.nextLine();

        return new School(name, address, city, postalCode);
    }

    public School (String name, String address, String city, String postalCode){
        this.name = name;
        this.address = address;
        this.city = city;
        this.postalCode = postalCode;
    }

    public String getName(){
        return name;
    }

    public String getAddress(){
        return address;
    }

    public String getCity(){
        return city;
    }

    public String getPostalCode(){
        return postalCode;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setAddress(String address){
        this.address = address;
    }

    public void setCity(String city){
        this.city = city;
    }

    public void setPostalCode(String postalCode){
        this.postalCode = postalCode;
    }

    public void printSchool(){
        System.out.println(this.name);
        System.out.println("adreça: " + this.address);
        System.out.println("codipostal: " + this.postalCode);
        System.out.println("ciutat: " + this.city);
    }
}
