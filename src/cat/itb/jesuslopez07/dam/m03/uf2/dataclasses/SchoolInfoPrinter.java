package cat.itb.jesuslopez07.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class SchoolInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        School school = School.readSchool(scanner);
        school.printSchool();
    }
}
