package cat.itb.jesuslopez07.dam.m03.uf2.dataclasses;

public class Product {
    private String name;
    private double prize;

    /**
     * Product constructor. Creates a product given the name and prize of the product.
     * @param name Name of the product.
     * @param prize Prize of the product.
     */

    public Product(String name, double prize){
        this.name = name;
        this.prize = prize;
    }

    //Getters:
    public String getName(){
        return this.name;
    }
    public double getPrize(){
        return this.prize;
    }

    //Setters:
    public void setName (String name){
        this.name = name;
    }
    public void setPrize(double prize){
        this.prize = prize;
    }
}
