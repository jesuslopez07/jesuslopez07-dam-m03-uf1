package cat.itb.jesuslopez07.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class GradeCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int numeroEstudiantes = scanner.nextInt();

        StudentStats[] notasEstudiantes = new StudentStats[numeroEstudiantes];

        for (int i = 0; i < notasEstudiantes.length; i++) {
            String nombre = scanner.next();
            double notaEjercicio = scanner.nextDouble();
            double notaExamen = scanner.nextDouble();
            double notaProyecto = scanner.nextDouble();
            notasEstudiantes[i] = new StudentStats(nombre, notaEjercicio, notaExamen, notaProyecto);
        }

        for (int i = 0; i < notasEstudiantes.length; i++) {
            System.out.printf("%s: %.1f\n", notasEstudiantes[i].getName(), notasEstudiantes[i].getNotaFinal());
        }
    }
}
