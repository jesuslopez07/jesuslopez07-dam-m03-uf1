package cat.itb.jesuslopez07.dam.m03.uf2.dataclasses;

import java.util.*;

public class Treballador {
    private String nom;
    private int sou;

    public Treballador(String nom, int sou){
        this.nom = nom;
        this.sou = sou;
    }

    public int getSou(){
        return this.sou;
    }
    public String getNom(){
        return this.nom;
    }

    public void setSou(int sou){
        this.sou = sou;
    }
    public void setNom(String nom){
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Treballador{" +
                "nom='" + nom + '\'' +
                ", sou=" + sou +
                '}';
    }
}
