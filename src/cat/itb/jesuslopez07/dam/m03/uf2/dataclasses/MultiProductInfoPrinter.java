package cat.itb.jesuslopez07.dam.m03.uf2.dataclasses;

import java.util.Locale;
import java.util.Scanner;

public class MultiProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        int numProductos = scanner.nextInt();
        Product[] productos = new Product[numProductos];

        String nombre = null;
        double precio = 0;

        for (int i = 0; i < productos.length; i++) {
            nombre = scanner.next();
            precio = scanner.nextDouble();
            productos[i] = new Product(nombre, precio);
        }
        for (int i = 0; i < productos.length; i++) {
            System.out.printf("El producto %s cuesta %.2f€\n", productos[i].getName(), productos[i].getPrize());
        }
    }
}
