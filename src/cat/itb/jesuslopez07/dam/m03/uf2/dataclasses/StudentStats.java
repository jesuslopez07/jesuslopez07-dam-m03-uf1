package cat.itb.jesuslopez07.dam.m03.uf2.dataclasses;

public class StudentStats {
    private String name;
    private double notaExercicis;
    private double notaExamen;
    private double notaProyecto;

    /**
     * Constructor from StudetnStats. Where you can find every single detail from any student.
     * @param name Name of the student.
     * @param notaExercicis Exercise mark of the student.
     * @param notaExamen Exam mark of the student.
     * @param notaProyecto Project mark of the student.
     */

    public StudentStats (String name, double notaExercicis, double notaExamen, double notaProyecto){
        this.name = name;
        this.notaExercicis = notaExercicis;
        this.notaExamen = notaExamen;
        this.notaProyecto = notaProyecto;
    }

    //Getters:

    public String getName() {
        return name;
    }

    public double getNotaExercicis() {
        return notaExercicis;
    }

    public double getNotaExamen() {
        return notaExamen;
    }

    public double getNotaProyecto() {
        return notaProyecto;
    }

    //Setters:

    public void setName(String name) {
        this.name = name;
    }

    public void setNotaExercicis(double notaExercicis) {
        this.notaExercicis = notaExercicis;
    }

    public void setNotaExamen(double notaExamen) {
        this.notaExamen = notaExamen;
    }

    public void setNotaProyecto(double notaProyecto) {
        this.notaProyecto = notaProyecto;
    }

    public double getNotaFinal(){
        double notaFinal = 0.3*this.notaExercicis + 0.3*this.notaExamen + 0.4*this.notaProyecto;
        return notaFinal;
    }

}
