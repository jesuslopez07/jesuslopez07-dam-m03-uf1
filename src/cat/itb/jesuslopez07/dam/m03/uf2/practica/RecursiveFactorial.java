package cat.itb.jesuslopez07.dam.m03.uf2.practica;

import java.util.Scanner;

public class RecursiveFactorial {

    public static int FactorialCalculator(int numero){
        if(numero > 0){
            int resultatFactorial = numero * FactorialCalculator(numero-1);
            return resultatFactorial;
        }
        return 1;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int nFactoriales = scanner.nextInt();

        int[] factoriales = new int[nFactoriales];


        for (int i = 0; i < factoriales.length; i++) {
            int numero = scanner.nextInt();
            System.out.println(RecursiveFactorial.FactorialCalculator(numero));
        }

    }
}
