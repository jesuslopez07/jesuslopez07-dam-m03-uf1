package cat.itb.jesuslopez07.dam.m03.uf2.practica;

import java.util.Scanner;

public class HighPeaks {

    private String nom;
    private int alçada;
    private String pais;
    private double distancia;
    private double temps;

    //Constructor:
    /**
     * Constructor HighPeaks...
     * @param nom Se refiere al nombre del pico.
     * @param alçada Se refiere a la altura del pico.
     * @param pais Se refiere al pais donde se situa el pico.
     * @param distancia Se refiere a la distancia recorrida.
     * @param temps Se refiere al tiempo del recorrido.
     */
    public HighPeaks(String nom, int alçada, String pais, double distancia, double temps){
        this.nom = nom;
        this.alçada = alçada;
        this.pais = pais;
        this.distancia = distancia;
        this.temps = temps;
    }

    //getters:
    public String getNom() {
        return this.nom;
    }

    public int getAlçada() {
        return this.alçada;
    }

    public String getPais() {
        return this.pais;
    }

    public double getDistancia() {
        return this.distancia;
    }

    public double getTemps() {
        return this.temps;
    }

    //metode per accedir a la velocitat, tempsHora, tempsMin...
    public double getVelocitat(){
        return (distancia/1000) / (temps/60);
    }
    public double getTempsHora(){
        if (temps >= 60){
            return temps/60;
        }
        return 0;
    }
    public double getTempsMin(){
        if(temps > 60){
            return temps%60;
        }
        if(temps == 60){
            return 0;
        }
        return temps;
    }

    //Metode per realitzar l'array dels cims...
    public static HighPeaks readPeaks(Scanner scanner){
        String nom = scanner.nextLine();
        int alçada = scanner.nextInt();
        String pais = scanner.next();
        double distancia = scanner.nextInt();
        double temps = scanner.nextInt();
        return new HighPeaks(nom, alçada, pais, distancia, temps);
    }

    //Metode de retorn de la vel max i nom del lloc:
    public static int llocVelMax(HighPeaks[] cimas){
        double velMax = 0;
        double vel;
        int llocVelMax = 0;
        for (int i = 0; i < cimas.length; i++) {
            vel = cimas[i].getVelocitat();
            if(vel > velMax){
                velMax = vel;
                llocVelMax = i;
            }
        }
        return llocVelMax;
    }

    public static double velMax(HighPeaks[] cimas){
        double velMax = 0;
        double vel;
        for (int i = 0; i < cimas.length; i++) {
            vel = cimas[i].getVelocitat();
            if(vel > velMax){
                velMax = vel;
            }
        }
        return velMax;
    }

    //Metodes per trobas la cima més alta i el seu nom:
    public static int cimaMasAlta(HighPeaks[] cimas){
        int alçada;
        int alçadaMesAlta = 0;
        for (int i = 0; i < cimas.length; i++) {
            alçada = cimas[i].getAlçada();
            if(alçada > alçadaMesAlta){
                alçadaMesAlta = alçada;
            }
        }
        return alçadaMesAlta;
    }

    public static int nomCimaMasAlta(HighPeaks[] cimas){
        int alçada;
        int alçadaMesAlta = 0;
        int nomCimAlt = 0;
        for (int i = 0; i < cimas.length; i++) {
            alçada = cimas[i].getAlçada();
            if(alçada > alçadaMesAlta){
                alçadaMesAlta = alçada;
                nomCimAlt = i;
            }
        }
        return nomCimAlt;
    }

    //Main
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numMax = scanner.nextInt();
        scanner.nextLine();
        HighPeaks[] cimas = new HighPeaks[numMax];

        for (int i = 0; i < cimas.length; i++) {
           HighPeaks cima = HighPeaks.readPeaks(scanner);
           cimas[i] = cima;
           scanner.nextLine();
        }

        System.out.println("------------------------\n" +
                "--- Cims aconseguits ---\n" +
                "------------------------");

        for (int i = 0; i < cimas.length; i++) {
            System.out.printf("%s - %s (%dm) - Temps: %01.0f:%02.0f\n", cimas[i].getNom(), cimas[i].getPais(),
                    cimas[i].getAlçada(), cimas[i].getTempsHora(), cimas[i].getTempsMin());
        }
        System.out.println("------------------------");
        System.out.println("N. cims: " + numMax);
        int nomCimAlt = HighPeaks.nomCimaMasAlta(cimas);
        int alçadaMesAlta = HighPeaks.cimaMasAlta(cimas);
        int llocVelMax = HighPeaks.llocVelMax(cimas);
        double velMax = HighPeaks.velMax(cimas);
        System.out.printf("Cim més alt: %s (%dm)\n", cimas[nomCimAlt].getNom(), alçadaMesAlta);
        System.out.printf("Cim més ràpid: %s (%01.0f:%02.0f)\n", cimas[llocVelMax].getNom(),
                cimas[llocVelMax].getTempsHora(), cimas[llocVelMax].getTempsMin());
        System.out.printf("Cim més veloç: %s %.2fkm/hora\n", cimas[llocVelMax].getNom(), velMax);
        System.out.print("------------------------");
    }
}
