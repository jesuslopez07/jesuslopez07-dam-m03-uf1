package cat.itb.jesuslopez07.dam.m03.uf2.practica;

import java.util.Scanner;

public class RecursiveFibonacci {

    public static int Fibonacci(int x){
        if (x != 0){
            if (x == 1 || x == 2){
                return 1;
            }else
                return Fibonacci(x-1) + Fibonacci(x-2);
        }
        return 1;
    }

    public static boolean isValidFiboNumber(int x){
        boolean isValidFNumber = false;
        int[] fibonacciNumbers = new int[20];
        for (int i = 0; i < fibonacciNumbers.length; i++) {
            fibonacciNumbers[i] = RecursiveFibonacci.Fibonacci(i+1);
        }
        for (int i = 0; i < fibonacciNumbers.length; i++) {
            if (fibonacciNumbers[i] == x){
                isValidFNumber = true;
            }
        }
        return isValidFNumber;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int numero = scanner.nextInt();

        System.out.println(isValidFiboNumber(numero));
    }
}
