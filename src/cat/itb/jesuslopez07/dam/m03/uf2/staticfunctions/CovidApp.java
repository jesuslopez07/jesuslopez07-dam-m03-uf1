package cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class CovidApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> list = CovidCalculations.readDailyCasesFromScanner(scanner);

        int totalNumberCovidCases = CovidCalculations.countTotalCases(list);

        double averageFromTotalCases = CovidCalculations.average(list);

        List<Double> growth = CovidCalculations.growthRates(list);

        double ultimCreix = growth.get(growth.size()-1);

        System.out.printf("Hi ha hagut %d casos en total, amb una mitjana de %.2f per dia.\n" +
                "L'útlim creixement és de %.2f.\n", totalNumberCovidCases, averageFromTotalCases, ultimCreix);
    }
}
