package cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

import static cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions.IntegerLists.*;

public class OldestStudent {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = readIntegerList(scanner);
        int maxEdat = max(list);
        System.out.printf("L'alumne més gran té %d anys \n", maxEdat);
    }
}
