package cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {
    public static List<Integer> readIntegerList(Scanner scanner){
        List<Integer> list = new ArrayList<>();
        while(true){
            int num = scanner.nextInt();
            if (num == -1){
                break;
            }
            list.add(num);
        }
        return list;
    }
    public static int min(List<Integer> list){
        int minimValor = list.get(0);
        for (int i = 1; i < list.size(); i++){
            if (list.get(i) < minimValor){
                minimValor = list.get(i);
            }
        }
        return minimValor;
    }
    public static int max(List<Integer> list){
        int maxim = list.get(0);
        for (int i = 1; i < list.size(); i++){
            if (list.get(i) > maxim){
                maxim = list.get(i);
            }
        }
        return maxim;
    }
    public static double average (List <Integer> list){
        int sumatori = 0;
        for (int i = 0; i < list.size(); i++){
            int suma = sumatori + list.get(i);
            sumatori = suma;
        }
        /*
        Tambien se le puede utilizar el "for each":
        for(int n : list){
            sumatori += n;
         */
        double mitjana = (double) sumatori / list.size();
        /*((double) sumatori) / list.size() -> esto lo que hara es que
        el double solo afectara a "sumatori". En canvio, si ponemos unicamente (double) delante del comando
        como hemos hecho en el programa. Lo que realizara es que el resto de la linia se le aplicara el double.
        Es por eso que es importante aplicar los parentesis para especificar el qué queramos aplicarle el double.
         */
        return mitjana;
    }

}
