package cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

import static cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions.IntegerLists.min;
import static cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions.IntegerLists.readIntegerList;

public class CheapestPrice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = readIntegerList(scanner);
        int minimValor = min(list);
        System.out.printf("El producte més econòmic val: %d€ \n", minimValor);
    }
}
