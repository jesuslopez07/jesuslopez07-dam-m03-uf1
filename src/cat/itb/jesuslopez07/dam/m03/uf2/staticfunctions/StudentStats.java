package cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

import static cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions.IntegerLists.*;

public class StudentStats {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = readIntegerList(scanner);
        int notaMin = min(list);
        int notaMax = max(list);
        double mitjana = average(list);
        System.out.printf("Nota mínima: %d\nNota màxima: %d\nNota mitjana: %.2f\n", notaMin, notaMax, mitjana);
    }
}
