package cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

import static cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions.IntegerLists.*;

public class AvgTemperature {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = readIntegerList(scanner);
        double mitjana = average(list);
        System.out.printf("Ha fet %.2f graus de mitjana\n", mitjana);
    }
}
