package cat.itb.jesuslopez07.dam.m03.uf2.staticfunctions;

import java.util.Scanner;

public class PersonValidator {
    /**
     * Returns true if is a valid phone number (composed only by digits, spaces or +)
     * @param phone
     * @return true if valid
     */
    public static boolean isValidPhoneNumber(String phone){

        boolean isValidNumber = true;

        for (int i = 0; i < phone.length(); i++){
            if(!Character.isDigit(phone.charAt(i)) && !(phone.charAt(i) == ' ') && !(phone.charAt(i) == '+')) {
                isValidNumber = false;
            }
        }

        return isValidNumber;
    }

    /**
     * Returns true if is a valid person name (composed only characters and starts with upper case)
     * @param personName
     * @return true if valid
     */
    public static boolean isValidPersonName(String personName){
        boolean isValidPersonName = true;

        for (int i = 0; i < personName.length(); i++) {
            if (!Character.isLetter(personName.charAt(i))){
                isValidPersonName = false;
            }
        }

        for (int i = 0; i < personName.length(); i++) {
            if (!Character.isUpperCase(personName.charAt(0))){
                isValidPersonName = false;
            }
        }

        for (int i = 1; i < personName.length(); i++) {
            if(!Character.isLowerCase(personName.charAt(i))){
                isValidPersonName = false;
            }
        }

        return isValidPersonName;
    }

    /**
     * Returns true if is a valid dni (including correct letter)
     * @param
     * @return true if valid
     */
    public static boolean isValidDni(String dni){
        if (dni.length() != 9){
            return false;
        }

        for(int i = 0; i < dni.length()-1; i++){
            if(!Character.isDigit(dni.charAt(i))){
                return false;
            }
        }

        for (int i = 0; i < dni.length(); i++){
            if(!Character.isLetter(dni.charAt(dni.length()-1))){
                return false;
            }
        }

        String numero = dni.substring(0,8);
        int numeroInt = Integer.parseInt(numero);
        int posicion = numeroInt%23;
        char[] lletraDNI = {
                'T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D',
                'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'
        };

        return lletraDNI[posicion] == dni.charAt(8);
    }

    /**
     * Returns true if is a valid postalCode
     * @param postalCode
     * @return true if valid
     */
    public static boolean isValidPostalcode(String postalCode){
        boolean isValidPostalCode = true;

        for(int i = 0; i < postalCode.length(); i++){
            if(!Character.isDigit(postalCode.charAt(i))){
                isValidPostalCode = false;
            }
        }
        return isValidPostalCode;
    }

    //Ejecucion de ejemplos:
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Introduzca su nombre:");
        String nom = scanner.nextLine();

        System.out.println("Introduzca su dni:");
        String dni = scanner.nextLine();

        System.out.println("Introduzca su numero de telefono:");
        String telefono = scanner.nextLine();

        System.out.println("Introduzca su codigo postal:");
        String codiPostal = scanner.nextLine();

        boolean isValidData = PersonValidator.isValidPersonName(nom) == true && PersonValidator.isValidDni(dni) == true &&
                PersonValidator.isValidPhoneNumber(telefono) == true && PersonValidator.isValidPostalcode(codiPostal) == true;

        if (isValidData == false){
            System.out.println("Hi ha alguna dada incorrecta");
        }else
            System.out.println("Dades correctes");
    }
}
