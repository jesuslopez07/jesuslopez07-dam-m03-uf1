package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf5;

import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Scanner;

public class VehicleITV {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        Queue<Vehiculo> vehiculos = new LinkedList<>();
        while(true){
            String option = scanner.nextLine();
            if (option.equals("NEW")){
                Vehiculo v = Vehiculo.readVehiculo(scanner);
                vehiculos.add(v);
                System.out.println(v);
            }
            if (option.equals("NEXT")){
                Vehiculo next = vehiculos.poll();
                if (next == null){
                    System.out.println("No hi ha cap vehicle esperant a ser revisat.");
                }else
                    System.out.printf("Següent: %s\n", next);
            }
            if (option.equals("END")){
                System.out.println();
                break;
            }
        }
    }
}
