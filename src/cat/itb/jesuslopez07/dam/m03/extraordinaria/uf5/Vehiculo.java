package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf5;

import java.util.Scanner;

public class Vehiculo {
    private String mat;
    private String marca;
    private String model;
    private int anyMat;
    private String tipoComb;

    public Vehiculo(String mat, String marca, String model, int anyMat, String tipoComb) {
        this.mat = mat;
        this.marca = marca;
        this.model = model;
        this.anyMat = anyMat;
        this.tipoComb = tipoComb;
    }

    public String getMat() {
        return mat;
    }

    public String getMarca() {
        return marca;
    }

    public String getModel() {
        return model;
    }

    public int getAnyMat() {
        return anyMat;
    }

    public String getTipoComb() {
        return tipoComb;
    }

    public static Vehiculo readVehiculo(Scanner scanner){
        String mat = scanner.next();
        String marca = scanner.next();
        String model = scanner.next();
        int anyMat = scanner.nextInt();
        String tipoComb = scanner.next();
        return new Vehiculo(mat, marca, model, anyMat, tipoComb);
    }

    @Override
    public String toString() {
        return String.format("%s - %s %s (%d, %s)", getMat(), getMarca(), getModel(), getAnyMat(), getTipoComb());
    }
}
