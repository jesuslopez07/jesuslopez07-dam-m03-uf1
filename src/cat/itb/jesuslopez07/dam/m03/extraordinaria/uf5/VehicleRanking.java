package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf5;

import java.util.*;

public class VehicleRanking {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        int n = scanner.nextInt();
        scanner.nextLine();

        List<Vehiculo> vehiculos = new ArrayList<>();
        for (int i = 0; i < n; i++){
            Vehiculo v = Vehiculo.readVehiculo(scanner);
            vehiculos.add(v);
        }

        //Vehicles ordenats per marca. I model si son de la mateixa.
        System.out.println("### Vehicles ordenats per marca");
        vehiculos.stream()
                .sorted(Comparator.comparing(Vehiculo::getMarca, String.CASE_INSENSITIVE_ORDER)
                        .thenComparing(Vehiculo::getModel))
                        .forEach(System.out::println);
        System.out.println();

        //Vehicles que han de portat etiq B perque mat a partir de 2006.
        System.out.println("### Vehicles amb etiqueta B");
        vehiculos.stream()
                .filter(vehiculo -> vehiculo.getAnyMat() >=2006 && vehiculo.getTipoComb().equals("Diesel"))
                .forEach(System.out::println);
        System.out.println();

        //Ultimo bmw creado.
        System.out.println("### Últim BWM");
        Vehiculo v = Collections.max(vehiculos, Comparator.comparing(Vehiculo::getAnyMat));
        System.out.println(v);
        System.out.println();

        //Numero de mat del 2017
        System.out.println("### Nombre de cotxes matriculats el 2017");
        System.out.println(vehiculos.stream().filter(v1 -> v1.getAnyMat() == 2017).count());
    }
}
