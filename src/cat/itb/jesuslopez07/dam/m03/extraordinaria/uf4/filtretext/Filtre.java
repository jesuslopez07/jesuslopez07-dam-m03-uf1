package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf4.filtretext;

public interface Filtre {
    String filterText(String text);
}
