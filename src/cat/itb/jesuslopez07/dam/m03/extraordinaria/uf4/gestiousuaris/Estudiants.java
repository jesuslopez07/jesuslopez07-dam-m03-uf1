package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf4.gestiousuaris;

import java.util.Locale;

public class Estudiants extends Usuaris{
    private int dataMat;

    public Estudiants(String nom, String cog1, String cog2, String dataNaix, int dataMat){
        super(nom, cog1, cog2, dataNaix);
        this.dataMat = dataMat;
    }

    @Override
    public String getNom() {
        return super.getNom();
    }

    @Override
    public String getCog1() {
        return super.getCog1();
    }

    @Override
    public String getCog2() {
        return super.getCog2();
    }

    @Override
    public String getDataNaix() {
        return super.getDataNaix();
    }

    public int getDataMat() {
        return dataMat;
    }

    public String getCorreu(){
        return String.format("%s.%s.%s@itb.cat", getNom().toLowerCase(), getCog1().toLowerCase(), Integer.toHexString(getDataMat()));
    }

    public String toString() {
        return String.format("Nom: %s\n" +
                "Cognoms: %s %s\n" +
                "Data de naixement: %s\n" +
                "Correu electrònic: %s \n", getNom(), getCog1(), getCog2(), getDataNaix(), getCorreu());
    }
}
