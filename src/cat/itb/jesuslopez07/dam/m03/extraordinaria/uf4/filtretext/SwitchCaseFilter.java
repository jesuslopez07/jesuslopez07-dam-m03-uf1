package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf4.filtretext;

public class SwitchCaseFilter implements Filtre {
    @Override
    public String filterText(String text) {
        for (int i = 0; i < text.length(); i++) {
            char letra = text.charAt(i);
            if (i % 2 == 0){
                text.valueOf(letra).toLowerCase();
            }else
                text.valueOf(letra).toUpperCase();
        }
        return text;
    }
}
