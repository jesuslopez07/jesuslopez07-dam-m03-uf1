package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf4.filtretext;

public class ReverseFilter implements Filtre {
    @Override
    public String filterText(String text) {
        String invertedText = "";
        for (int i = text.length() -1; i >= 0; i--) {
            invertedText += text.charAt(i);
        }
        return invertedText;
    }
}
