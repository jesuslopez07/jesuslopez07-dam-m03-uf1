package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf4.filtretext;

public class UpperCaseFilter implements Filtre {
    @Override
    public String filterText(String text) {
        return text.toUpperCase();
    }
}
