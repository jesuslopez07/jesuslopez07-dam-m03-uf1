package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf4.gestiousuaris;

public class GestioUsuaris {
    public static void main(String[] args) {
        Usuaris u1 = new Usuaris("Dani", "Carrasco", "Morera", "04/09/1986");
        Usuaris u2 = new Usuaris("Laura", "Ivars", "Perelló", "17/05/1996");
        Estudiants e1 = new Estudiants("Aitana", "Sanchis", "Marí", "20/11/2001", 2020);

        System.out.println(u1.toString());
        System.out.println(u2.toString());
        System.out.println(e1.toString());
    }
}
