package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf4.gestiousuaris;

public class Usuaris {
    private String nom;
    private String cog1;
    private String cog2;
    private String dataNaix;

    public Usuaris(String nom, String cog1, String cog2, String dataNaix) {
        this.nom = nom;
        this.cog1 = cog1;
        this.cog2 = cog2;
        this.dataNaix = dataNaix;
    }

    public String getNom() {
        return nom;
    }

    public String getCog1() {
        return cog1;
    }

    public String getCog2() {
        return cog2;
    }

    public String getDataNaix() {
        return dataNaix;
    }

    public String getCorreu(){
        return String.format("%s.%s@itb.cat", getNom().toLowerCase(), getCog1().toLowerCase());
    }

    @Override
    public String toString() {
        return String.format("Nom: %s\n" +
                "Cognoms: %s %s\n" +
                "Data de naixement: %s\n" +
                "Correu electrònic: %s \n", getNom(), getCog1(), getCog2(), getDataNaix(), getCorreu());
    }
}
