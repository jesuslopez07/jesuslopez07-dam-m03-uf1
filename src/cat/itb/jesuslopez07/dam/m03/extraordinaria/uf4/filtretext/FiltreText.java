package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf4.filtretext;

import java.awt.image.ComponentSampleModel;
import java.util.Scanner;
public class FiltreText {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        UpperCaseFilter upperCaseFilter = new UpperCaseFilter();
        System.out.println(upperCaseFilter.filterText(text));
        SwitchCaseFilter switchCaseFilter = new SwitchCaseFilter();
        System.out.println(switchCaseFilter.filterText(text));
        ReverseFilter reverseFilter = new ReverseFilter();
        System.out.println(reverseFilter.filterText(text));
    }
}
