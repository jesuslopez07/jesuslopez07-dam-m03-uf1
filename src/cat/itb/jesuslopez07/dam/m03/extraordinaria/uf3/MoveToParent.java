package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class MoveToParent {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String ruta = scanner.nextLine();
        Path file = Path.of(ruta);
        String home = System.getProperty("user.home");
        Path parentFile = Path.of(home);
        try{
            if(!Files.exists(file)){
                Files.createFile(file);
            }else
                Files.copy(file, parentFile);
                System.out.println("S'ha copiat el fitxer someFile.txt a la carpeta "+ parentFile);
        }catch (IOException io){
            System.out.println(io.getMessage());
        }
    }
}
