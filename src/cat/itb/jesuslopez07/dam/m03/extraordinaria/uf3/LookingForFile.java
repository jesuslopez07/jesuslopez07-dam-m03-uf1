package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf3;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class LookingForFile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String folderPath = scanner.nextLine();
        Path folder = Path.of(folderPath);

        String folderPath2 = scanner.nextLine();
        Path folder2 = Path.of(folderPath2);

        try{
            if(!Files.exists(folder)){
                Files.createFile(folder);
            }else
                try{
                    OutputStream outputStream = Files.newOutputStream(folder, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    PrintStream printStream = new PrintStream(outputStream, true);
                    if (!Files.exists(folder2)){
                        printStream.println("No s'ha trobat el fitxer\n");
                        System.out.println("No s'ha trobat el fitxer\n");
                    }else
                        printStream.println("S'ha trobat el fitxer\n");
                        System.out.println("S'ha trobat el fitxer\n");
                }catch (IOException io2){
                    System.out.println(io2.getMessage());
                }
        }catch (IOException io){
            System.out.println(io.getMessage());
        }
    }
}
