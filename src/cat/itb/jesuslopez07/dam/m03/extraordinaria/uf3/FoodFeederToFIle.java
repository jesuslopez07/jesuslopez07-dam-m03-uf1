package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf3;

import cat.itb.jesuslopez07.dam.m03.uf5.generalexam.Book;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FoodFeederToFIle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        scanner.nextLine();

        List<Animal> animals = new ArrayList<>();
        for (int i = 0; i < n; i++){
            Animal a = Animal.readAnimal(scanner);
            animals.add(a);
        }

        String path = scanner.nextLine();
        Path file = Path.of(path);

        try{
            if (!Files.exists(file)){
                Files.createFile(file);
            }else
                try{
                    OutputStream outputStream = Files.newOutputStream(file, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                    PrintStream printStream = new PrintStream(outputStream, true);
                    printStream.println("---------- Animals ----------");
                    for(Animal a : animals){
                        printStream.print(a.toString());
                    }
                }catch (IOException io){
                    System.out.println(io.getMessage());
                }
        }catch (IOException io){
            System.out.println(io.getMessage());
        }
    }
}
