package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf3;

import cat.itb.jesuslopez07.dam.m03.uf5.generalexam.Book;

import java.util.Scanner;

public class Animal {
    private String code;
    private String name;
    private int nApat;
    private int grams;

    public Animal(String code, String name, int nApat, int grams) {
        this.code = code;
        this.name = name;
        this.nApat = nApat;
        this.grams = grams;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public int getnApat() {
        return nApat;
    }

    public int getGrams() {
        return grams;
    }

    public int getTotal(){
        return grams*nApat;
    }

    public static Animal readAnimal(Scanner scanner){
        String code = scanner.next();
        String name = scanner.next();
        int nApat = scanner.nextInt();
        int grams = scanner.nextInt();
        scanner.nextLine();
        return new Animal(code, name, nApat, grams);
    }

    @Override
    public String toString() {
        return String.format("%s-%s: %d àpats, %d g/apat, total %dg\n", getCode(), getName(), getnApat(), getGrams(), getTotal());
    }
}
