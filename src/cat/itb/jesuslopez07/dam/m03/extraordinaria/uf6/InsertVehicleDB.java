package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf6;

import cat.itb.jesuslopez07.dam.m03.extraordinaria.uf5.Vehiculo;
import cat.itb.jesuslopez07.dam.m03.extraordinaria.uf6.daos.CarsDAO;
import cat.itb.jesuslopez07.dam.m03.extraordinaria.uf6.data.Database;

import java.util.Locale;
import java.util.Scanner;

public class InsertVehicleDB {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Database db = Database.getInstance();
        CarsDAO CarsDAO = new CarsDAO();
        db.connect();

        String mat = scanner.next();
        String marca = scanner.next();
        String model = scanner.next();
        int anyMat = scanner.nextInt();
        String tipoComb = scanner.next();
        Vehiculo v = new Vehiculo(mat, marca, model, anyMat, tipoComb);
        boolean ok = CarsDAO.insert(v);
        if(ok)
            System.out.println();
            System.out.println(v.toString() + "insertat");

        db.close();
    }
}
