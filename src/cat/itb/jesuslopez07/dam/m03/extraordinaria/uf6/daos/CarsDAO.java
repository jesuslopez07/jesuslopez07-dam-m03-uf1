package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf6.daos;

import cat.itb.jesuslopez07.dam.m03.extraordinaria.uf5.Vehiculo;
import cat.itb.jesuslopez07.dam.m03.extraordinaria.uf6.data.Database;


import java.lang.invoke.VarHandle;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CarsDAO {
    public boolean insert(Vehiculo v){
        try{
            Connection connection = Database.getInstance().getConnection();
            String query = "INSERT INTO cars(matricula, marca, model, any_matriculacio, combustible) VALUES(?, ?, ?, ?, ?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, v.getMat());
            insertStatement.setString(2, v.getMarca());
            insertStatement.setString(3, v.getModel());
            insertStatement.setInt(4, v.getAnyMat());
            insertStatement.setString(5, v.getTipoComb());
            insertStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public List<Vehiculo> listVehicleLisByYearFuel(int any_matriculacio, String combustible){
        List<Vehiculo> list = new ArrayList<>();
        try{
            Database db = Database.getInstance();
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT * FROM cars WHERE any_matriculacio = ? and combustible = ?";
            PreparedStatement listStatement = connection.prepareStatement(query);
            listStatement.setInt(1, any_matriculacio);
            listStatement.setString(2, combustible);
            ResultSet resultat = listStatement.executeQuery();

            while (resultat.next()){
                String mat = resultat.getString("matricula");
                String marca = resultat.getString("marca");
                String model = resultat.getString("model");
                int anyMat = resultat.getInt("any_matriculacio");
                String tipoComb = resultat.getString("combustible");
                list.add(new Vehiculo(mat, marca, model, anyMat, tipoComb));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return list;
    }

    public int vehicleCountBrand(String marca){
        int n = 0;
        try {
            Database db = Database.getInstance();
            Connection connection = Database.getInstance().getConnection();
            String query = "SELECT count(*) FROM cars WHERE marca = ?";
            PreparedStatement countStatement = connection.prepareStatement(query);
            countStatement.setString(1, marca);
            ResultSet resultat = countStatement.executeQuery();

        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return n;
    }
}
