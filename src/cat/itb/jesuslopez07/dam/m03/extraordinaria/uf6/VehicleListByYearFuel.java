package cat.itb.jesuslopez07.dam.m03.extraordinaria.uf6;

import cat.itb.jesuslopez07.dam.m03.extraordinaria.uf5.Vehiculo;
import cat.itb.jesuslopez07.dam.m03.extraordinaria.uf6.daos.CarsDAO;
import cat.itb.jesuslopez07.dam.m03.extraordinaria.uf6.data.Database;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class VehicleListByYearFuel {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Database db = Database.getInstance();
        CarsDAO CarsDAO = new CarsDAO();
        db.connect();

        int any_matriculacio = scanner.nextInt();
        String combustible = scanner.next();
        List<Vehiculo> books = CarsDAO.listVehicleLisByYearFuel(any_matriculacio, combustible);
        books.forEach(System.out::println);

        db.close();
    }
}
